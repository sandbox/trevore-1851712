<?php
/**
 * @file
 * ujumbe_ui_default.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ujumbe_ui_default_views_api() {
  return array("api" => "3.0");
}
