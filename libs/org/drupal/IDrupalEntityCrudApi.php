<?php
namespace org\drupal;
{// Begin namespace

/**
 * Interface for Entity CRUD operations.
 */
interface IDrupalEntityCrudApi {
  
  /**
   * Loads a single Entity.
   * @param int $anId - a single Entity ID to load.
   * @param boolean $bResetInternalCache - (optional) reset the internal cache.
   * @return Returns the loaded Entity.
   */
  static public function loadEntity($anId, $bResetInternalCache = FALSE);
  
  /**
   * Load multiple Entities based on certain conditions.
   * @param array $aIds - array of Entity IDs to load.
   * @param array $aConditions - array of conditions.
   * @param boolean $bResetInternalCache - (optional) reset the internal cache.
   * @return Returns an array of loaded Entities.
   */
  static public function loadEntities(array $aIds = array(), array $aConditions = array(), $bResetInternalCache = FALSE);
  
  /**
   * Save the Entity.
   * @param $anEntity
   * @return SAVED_NEW or SAVED_UPDATED is returned depending on the 
   *   operation performed.
   */
  static public function saveEntity($anEntity);
  
  /**
   * Deletes the Entity from the db.
   * In case of failures, an exception is thrown.
   * @param entity $anEntity - loaded entity to delete. 
   */
  static public function deleteEntity($anEntity);
  
  /**
   * Delete multiple Entities.
   * In case of failures, an exception is thrown.
   * @param array $aEntityIds - array of entity_id() results.
   */
  static public function deleteEntities(array $aEntityIds);
  
  
}// End interface

}// End namespace
