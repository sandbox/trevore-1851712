<?php
namespace org\drupal;
use \stdClass as BaseClass;
{// Begin namespace

/**
 * Abstract class for Node-based data models.
 */
abstract class NodeUtils extends BaseClass {

  /**
   * Return the parameter's contents as a debug string.
   * @param mixed $var - var to dump
   * @param boolean $bSingleLine - replaces all newlines with space,
   *     defaults to TRUE.
   * @return string Captures var_dump() and reports it back as a string.
   */
	static public function debugStr($var, $bSingleLine = TRUE) {
		ob_start();
		var_dump($var);
		if ($bSingleLine) {
		  return str_replace("\n",' ',ob_get_clean());
		}
		else {
		  return ob_get_clean();
		}
	}

  /**
   * Executes the supplied Node query and loads the result set
   * into an array of Node objects.
   * 
   * @param EntityFieldQuery $aNodeQuery
   *   The Node query to execute.
   * 
   * @return
   *   A Drupal loaded entity object array of Node object results.
   */
  static public function executeNodeQueryAndLoadResults($aNodeQuery) {
    if (isset($aNodeQuery)) {
      $theResults = $aNodeQuery->execute();
      if (isset($theResults['node'])) {
        return entity_load('node', array_keys($theResults['node']));
      }
    } 
  }
  
  /**
   * Gets the field data without rendering it for display.
   *
   * @param $aNode
   *   Loaded Node object containing the field data.
   *
   * @param string $aFieldName
   *   The field name to retrieve.
   *
   * @param number $aDelta (optional)
   *   The revision number of the field data to display.
   */
  static public function getNodeField($aNode, $aFieldName, $aDelta=0) {
  	$theFieldData = field_get_items('node', $aNode, $aFieldName);
  	//echo (print_r($theFieldData));
  	if (is_array($theFieldData)) {
  		return $theFieldData[$aDelta];
  	} else {
  		return $theFieldData;
  	}
  }
  
  /**
   * Render the field data without any labels, but themed appropriately.
   *
   * @param $aNode
   *   Loaded Node object containing the field data to display.
   *
   * @param string $aFieldName
   *   The field name to display.
   *
   * @param string/array $aDisplay (optional)
   *   Can be either the name of a view mode, or an array of display settings.
   *   See field_view_field() for more information.
   *
   * @param number $aDelta (optional)
   *   The revision number of the field data to display.
   */
  static public function renderNodeField($aNode, $aFieldName, $aDisplay=array(), $aDelta=0) {
  	$theFieldData = self::getNodeField($aNode, $aFieldName, $aDelta);
  	$theDisplayData = field_view_value('node', $aNode, $aFieldName, $theFieldData , $aDisplay);
  	return render($theDisplayData);
  }
  
  /**
   * Get a new Drupal Node of bundle type $aNodeType.
   * @param string $aNodeType - the node bundle name.
   * @return \stdClass Returns the Drupal Node object.
   * /
  static public function getNewNode($aNodeType) {
    $theNode = new DrupalNode();
    $theNode->type = $aNodeType;
    node_object_prepare($theNode);
    return $theNode;
  }
  */

  /**
   * Creates a new Node of type $aBundleName.
   * @param string $aBundleName - the node bundle name to create.
   * @return Returns the \EntityMetadataWrapper defined by Entity API.
   */
  static public function createNode($aBundleName) {
    global $user;
    return entity_metadata_wrapper('node',entity_create('node',
        array('type' => $aBundleName, 'uid' => $user->uid)));
  }
  
  /**
   * Gets the Node given the nid.
   * @param int $aNodeId - NID to load from db.
   * @return Returns the \EntityMetadataWrapper or NULL if not found.
   */
  static public function getNode($aNodeId) {
    return entity_metadata_wrapper('node', $aNodeId);
  }
  
  /**
   * Permanently save an Entity API node.
   * In case of failures, an exception is thrown.
   * @param \EntityDrupalWrapper $aNode - The node to save.
   * @return SAVED_NEW or SAVED_UPDATED is returned depending on the 
   *   operation performed.
   */
  static public function saveNode($aNode) {
    return entity_save('node',$aNode);
  }
    
}// End class

}// End namespace
