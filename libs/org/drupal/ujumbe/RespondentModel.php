<?php
namespace org\drupal\ujumbe;
use \org\drupal\BaseNodeModel as BaseClass;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\RespondentGroupModel as UjumbeGroups;
{// Begin namespace

/**
 * Model class for Ujumbe Respondents.
 */
class RespondentModel extends BaseClass {
  const BUNDLE_NAME = 'ujumbe_respondent';
  const BUNDLE_AS_URL = 'ujumbe-respondent';

  /**
   * @return \org\drupal\ujumbe\RespondentModel
   */
  static public function getNew() {
    return new RespondentModel();
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getBundleName()
   */
  public function getBundleName() {
    return self::BUNDLE_NAME;
  }
  
  /**
   * Gets our custom content type info for Drupal.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal custom content type info.
   */
  static public function getContentTypeInfo($t) {
    // Drupal 7 wants content type information as an associative array
    return array(
        'name' => $t('Ujumbe Respondent'),
        'base' => 'node_content',
        'description' => $t('People or phone numbers that have sent or received a Ujumbe message.'),
        'has_title' => '1',
        'title_label' => $t('Phone'),
        'locked' => FALSE,
        'body_label' => $t('Notes'),
    );
  }

  /**
   * @return Returns the installed fields info for our content type.
   */
  public function getFieldDefinitions() {
    // how to define checkbox fields: http://drupal.org/node/1339968
    return array(
        // defining "body" field in case its not there for some reason
        'body' => $this->getDefaultBodyFieldDefinition(),
        'ujumbe_name' => array(
            'field_name'  => 'ujumbe_name',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 255,
            ),
        ),
        'ujumbe_gender' => array(
            'field_name'  => 'ujumbe_gender',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 10,
            ),
        ),
        'ujumbe_age' => array(
            'field_name'  => 'ujumbe_age',
            'type'        => 'number_integer',
        ),
        'ujumbe_location' => array(
            'field_name'  => 'ujumbe_location',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 32,
            ),
        ),
        'ujumbe_group_nid' => $this->getNodeIdFieldDefinition('ujumbe_group_nid', 
            UjumbeGroups::BUNDLE_NAME, TRUE) + array(
            'cardinality' => FIELD_CARDINALITY_UNLIMITED,
        ),
        'ujumbe_external_identifier' => array(
            'field_name'  => 'ujumbe_external_identifier',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 255,
            ),
        ),
    );
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getDbSchema()
   */
  public function getDbSchema($aFieldInfo) {
    return array();
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getFieldInstances()
   */
  public function getFieldInstances($t) {
    return array(
        'ujumbe_name' => $this->getFieldInstance('ujumbe_name', array(
            'label' => $t('Name'),
            'widget' => array('type' => 'text_textfield',),
        )),
        'ujumbe_gender' => $this->getFieldInstance('ujumbe_gender', array(
            'label' => $t('Gender'),
            'widget' => array('type' => 'text_textfield',),
        )),
        'ujumbe_age' => $this->getFieldInstance('ujumbe_age', array(
            'label' => $t('Age'),
            'widget' => array('type' => 'number',),
        )),
        'ujumbe_location' => $this->getFieldInstance('ujumbe_location', array(
            'label' => $t('Location'),
            'widget' => array('type' => 'text_textfield',),
        )),
        'ujumbe_group_nid' => $this->getNodeIdFieldInstance('ujumbe_group_nid', array(
            'label' => $t('Groups'),
        )),
        'ujumbe_external_identifier' => $this->getFieldInstance('ujumbe_external_identifier', array(
            'label' => $t('Identifier'),
            'widget' => array('type' => 'text_textfield',),
        )),
        'body' => $this->getBodyFieldInstance($t, array(
            'label' => $t('Notes'),
        )),
    );
  }
  
  /**
   * Get all of the respondents in the database
   *
   * @param int $aPageNum
   *   (optional) Filter the respondents to this page number.
   *
   * @return
   *   A Drupal loaded entity object array of Node object results.
   */
  public function getRespondents($aPageNum = -1) {
    $theNodeQuery = $this->getNewNodeQuery($aPageNum)->propertyOrderBy('nid');
    return NodeUtils::executeNodeQueryAndLoadResults($theNodeQuery);
  }
  
  /**
   * Return the total number of respondents.
   *
   * @return
   *   The number of respondents.
   */
  public function getCount() {
    return $this->getNewNodeQuery()->count()->execute();
  }

  /**
   * Gets the node ID if $aPhone exists in db, else -1.
   * @param string $aPhone - phone to search for in db.
   * @return int Returns the NID of the phone, else -1.
   */
  public function getIdFromPhone($aPhone) {
    $thePhoneNumber = $this->sanitizePhoneNumber($aPhone);
    $theNodeQuery = $this->getNewNodeQuery();
    $theNodeQuery = $theNodeQuery->propertyCondition('title', $thePhoneNumber, '=');
    $theResultSet = $theNodeQuery->execute();
    if (isset($theResultSet['node'])) {
      reset($theResultSet['node']); // ensure we are at start of array
      return key($theResultSet['node']); // grab first key, which is a NID
      // by design, there should only ever be one result, but we still
      //   have an array returned and need to get the first key from it.
    } else {
      return -1;
    }
  }

  /**
   * Get a new Entity object filled with required fields.
   * @param string $aPhone - node title is required which is the phone.
   * @return Returns the new \EntityMetadataWrapper.
   */
  public function createNewEntity($aPhone) {
    $theNewEntity = NodeUtils::createNode(self::BUNDLE_NAME);
    $theNewEntity->title->set($this->sanitizePhoneNumber($aPhone));
    return $theNewEntity;
  }

  /**
   * Insert a new person into the database
   * 
   * @param string $aPhone - The phone number of the person
   * @param string $aLoc - (optional) The location of the person
   * @param string $aName - (optional) The name of the person
   * @param string $aIdentifier - (optional) The identifier of the person
   * @param string $aGender - (optional) The gender of the person
   * @param int $aAge - (optional) The age of the person
   * @param int $aGroupId - (optional) The group_id of the person
   * 
   * @return Returns the ID of the person inserted
   */
  public function insertRespondent($aPhone, $aLoc = null, $aName = null, $aIdentifier = null, 
      $aGender = null, $aAge = null, $aGroupId = 0) {
    // check to see if this number is already in our db
    $theResponderId = $this->getIdFromPhone($aPhone);
    if ($theResponderId < 0) {
      $theNewRespondent = $this->createNewEntity($aPhone);
      $theNewRespondent->ujumbe_location->set($aLoc);
      $theNewRespondent->ujumbe_name->set($aName);
      $theNewRespondent->ujumbe_external_identifier->set($aIdentifier);
      $theNewRespondent->ujumbe_gender->set($aGender);
      $theNewRespondent->ujumbe_age->set($aAge);
      if ($aGroupId > 0) {
        $theNewRespondent->ujumbe_group_nid[] = $aGroupId;
      }
      $theNewRespondent->save();
      $theResponderId = $theNewRespondent->getIdentifier();
    }
    return $theResponderId;
  }
  
  
}// End class

}// End namespace
