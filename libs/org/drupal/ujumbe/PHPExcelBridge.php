<?php
namespace org\drupal\ujumbe;
use \stdClass as BaseClass;
use \PHPExcel;
use \PHPExcel_IOFactory;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\RespondentModel as UjumbeRespondents;
use \org\drupal\ujumbe\TextMsgModel as UjumbeMessages;
use \org\drupal\ujumbe\CallMsgModel as UjumbeCalls;
{// Begin namespace

/**
 * Bridge class between PHPExcel library and Ujumbe.
 * PHPExcel is a required library for this class.
 * It should be located in sites/all/libraries/phpexcel
 *
 * The reason xls was chosen instead of a csv is because csv files 
 * technically do not support anything other then ASCII. So no UTF support.
 *
 * It can be downloaded from http://phpexcel.codeplex.com/releases/view/96183 
 *
 * For more info on PHPExcel go to {@link http://phpexcel.codeplex.com/}
 */
class PHPExcelBridge extends BaseClass {

  /**
   * May return NULL if PHPExcel library does not exist.
   * @return \org\drupal\ujumbe\PHPExcelBridge
   */
  static public function getNew() {
    if (self::libraryExists()) {
      return new PHPExcelBridge();
    }
  }

  /**
   * @return Return TRUE if library exists.
   */
  static public function libraryExists() {
    if (($library = libraries_detect('phpexcel')) && !empty($library['installed'])) {
      // The library is installed. Awesome!
      $theErrMsg = t('@name found!', array('@name' => $library['name']));
      drupal_set_message($theErrMsg,'status');
      return TRUE;
    }
    else {
      // Something went wrong. :(
      $theErrMsg = t('@name not found in sites/all/libraries', array('@name' => $library['name']));
      drupal_set_message($theErrMsg,'error');
      watchdog('ujumbe',$theErrMsg, array(), WATCHDOG_ERROR,$library['vendor url']);
    }
    return FALSE;     
  }

  /**
   * Exports a result set given a title and headers.
   * TODO this code only works for up to 26 columns, needs work for more than that.
   * 
   * The file is downloaded to the requesting computer and then the code exits.
   * So no new page is displayed.
   *
   * @param string $aTitle - Spreadsheet title.
   * @param array $aHeaders - column headers, if empty() they will be skipped.
   * @param \PDOStatement $aNodeSet - a db_query of node IDs that will be
   *     loaded one at a time.
   * @param function/method $aLoadNodeRow - given the NodeRow as Assoc, return
   *     the Assoc array containing the cells to export.
   * @param string $aFilename - OPTIONAL - downloaded filename; if NULL, 
   *     it will construct one based on the $aTitle parameter.
   */
  public function exportResultSet($aTitle, $aHeaders, $aNodeSet, $aLoadNodeRow, $aFilename=null) {
    if (empty($aTitle)) {
      $aTitle = 'Ujumbe Export';
    }
    if (empty($aFilename)) {
      $aFilename = str_replace(' ', '_', $aTitle) . date('Y-m-d-H-i') . '.xls';
    }
    
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setTitle($aTitle);
    //$theSpreadsheet->setActiveSheetIndex(0); is this line really needed?

    //TODO this code only works for up to 26 columns, needs work for more than that.
    
    $rowNumber = 1;
    if (!empty($aHeaders)) {
      $theCol = 'A';
      foreach((array)$aHeaders as $theColHeader) {
      	$objPHPExcel->getActiveSheet()->setCellValue($theCol++ . $rowNumber, $theColHeader);
      }
      $rowNumber += 1;
    }
    
    // Loop through the result set
    while (($theNodeRow = $aNodeSet->fetchAssoc()) !== FALSE) {
      $row = (!empty($aLoadNodeRow)) ? $aLoadNodeRow($theNodeRow) : $theNodeRow;
      $theCol = 'A';
    	foreach ($row as $cell) {
    		$objPHPExcel->getActiveSheet()->setCellValue($theCol++ . $rowNumber, $cell);
    	}
    	$rowNumber += 1;
    }
    
    // Freeze pane so that the heading line won't scroll
    if (!empty($aHeaders)) {
      $objPHPExcel->getActiveSheet()->freezePane('A2');
    }

    // Save as an Excel BIFF (xls) file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $aFilename . '"');
    header('Cache-Control: max-age=0');
    
    $objWriter->save('php://output');
    exit();
  }

  /**
   * Processing function for exporting Respondent nodes.
   * @param mixed $aNodeRow
   * @return array Returns an array used for export.
   */
  protected function loadRespondentRow($aNodeRow) {
    $theRow = array();
    if (!empty($aNodeRow)) {
      $theNode = node_load($aNodeRow->nid);
    	$theRow[] = $theNode->nid;
    	$theRow[] = $theNode->title;
    	$theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_name');
    	$theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_external_identifier');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_group_id');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_gender');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_age');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_location');
      $theRow[] = date('c', $project->created);
    }
    return $theRow;
  }
  
  /**
   * This function exports all of the people in the system into a excel file.
   *
   * The file is downloaded to the requesting computer and then the code exits.
   * So no new page is displayed.
   */
  public function exportRespondents() {
    $dbRespondents = UjumbeRespondents::getNew();
    // Gets all of the respondents in the database ordered by Phone Number
    $theQuery = db_select('node', 'n')->fields('n', array('nid', 'title'));
    $theQuery->condition('type', $dbRespondents->getBundleName(), '=');
    $theQuery->condition('status', 1, '=');
    $theQuery->orderBy('title');
    $theNodeSet = $theQuery->execute();

    // The header array matches what the loadRespondentRow will return.
  	$theHeaders = array(
  	    'Person ID',
  	    'Phone Number',
  	    'Name',
  	    'Identifier',
  	    'Group ID',
  	    'Gender',
  	    'Age',
  	    'Location',
  	    'Timestamp',
  	);
  	$this->exportResultSet('Ujumbe Respondents', $theHeaders, $theNodeSet, $this->loadRespondentRow);
  }
  
  /**
   * Processing function for exporting Message nodes.
   * @param mixed $aNodeRow
   * @return array Returns an array used for export.
   */
  protected function loadMessageRow($aNodeRow) {
    $theRow = array();
    if (!empty($aNodeRow)) {
      $theNode = node_load($aNodeRow->nid);
    	$theRow[] = $theNode->nid;
    	//$theRow[] = $theNode->title; DO NOT EXPORT THE PHONE NUMBER!
    	//TODO convert this code to use Entity API, since it work really work as is.
    	$theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_project_nid');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_responder_nid');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_message');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_translation');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_body?');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_source_id');
      $theRow[] = NodeUtils::getNodeField($theNode, 'ujumbe_external_identifier');
      $theFieldData = NodeUtils::getNodeField($theNode, 'ujumbe_xmit_status');
      $theRow[] = $theFieldData; // value of list_integer field
      $theRow[] = render(field_view_value('node', $theNode, 'ujumbe_xmit_status', $theFieldData , array()));
      $theRow[] = date('c', $project->created);
    }
    return $theRow;
  }
  
  public function exportMessages($aProjectId = -1, $aRespondentId = -1) {
    $dbMsgs = UjumbeMessages::getNew();
    if ($aProjectId > 0) {
      $dbMsgs->mProjectId = $aProjectId;
    }
    if ($aRespondentId > 0) {
      $dbMsgs->mResponderId = $aRespondentId;
    }
    // Gets all of the respondents in the database ordered by reverse create date
    $theNodeSet = $dbMsgs->getNewNodeQuery()->orderBy('created', 'DESC')->execute();
  	 
  	// The header array matches what the loadRespondentRow will return.
  	$theHeaders = array(
  	    'Message ID',
  	    'Project ID',
  	    'Person ID',
  	    'Message',
  	    'Translation',
  	    'Notes',
  	    'Source ID',
  	    'Source',
  	    'Source Table',
  	    'State ID',
  	    'State Name',
  	    'Timestamp',
  	);
  	$this->exportResultSet('Ujumbe Messages', $theHeaders, $theNodeSet, $this->loadMessageRow); 
  }  
  
}// End class

}// End namespace
