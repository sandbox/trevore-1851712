<?php
namespace org\drupal\ujumbe;
use \org\drupal\BaseNodeModel as BaseClass;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
{// Begin namespace

/**
 * Model class for Ujumbe Repondent Groups.
 * Entity API module required.
 */
class RespondentGroupModel extends BaseClass {
  const BUNDLE_NAME = 'ujumbe_group';
  const BUNDLE_AS_URL = 'ujumbe-group';
    
  /**
   * @return \org\drupal\ujumbe\RespondentGroupModel
   */
  static public function getNew() {
    return new RespondentGroupModel();
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getBundleName()
   */
  public function getBundleName() {
    return self::BUNDLE_NAME;
  }
  
  /**
   * Gets our custom content type info for Drupal.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal custom content type info.
   */
  static public function getContentTypeInfo($t) {
    // Drupal 7 wants content type information as an associative array
    return array(
        'name' => $t('Ujumbe Group'),
        'base' => 'node_content',
        'description' => $t('Respondent groupings.'),
        'has_title' => '1',
        'title_label' => $t('Title'),
        'locked' => FALSE,
        'body_label' => $t('Description'),
    );
  }
  
  /**
   * @return Returns the installed fields information.
   */
  public function getFieldDefinitions() {
    return array(
        // defining "body" field in case its not there for some reason
        'body' => $this->getDefaultBodyFieldDefinition(),
    );
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getDbSchema()
   */
  public function getDbSchema($aFieldInfo) {
    return array();
  }
  
  /**
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal field instance info for our content type.
   */
  public function getFieldInstances($t) {
    return array(
        'body' => $this->getBodyFieldInstance($t),
    );
  }
  
  /**
   * Modify the parameter to contain our hook_menu() info.
   * @param array $aMenuInfos - module menu info.
   */
  static public function buildMenuInfo(array &$aMenuInfos) {
    $aMenuInfos['ujumbe_groups'] = array(
        'title' => 'Ujumbe Groups',
        'page callback' => '_ujumbe_groups_html_page', //can make this reference a static function from this class
        'access arguments' => array('access ujumbe content'),
        'type' => MENU_NORMAL_ITEM,
    );
    $aMenuInfos['ujumbe_new_group'] = array(
        'title' => 'Ujumbe New Group',
        'page callback' => '_ujumbe_new_group_html_page',
        'access arguments' => array('access ujumbe admin'),
        'type' => MENU_CALLBACK,
    );
    $aMenuInfos['ujumbe_group_details'] = array(
        'title' => 'Ujumbe Group Details',
        'page callback' => '_ujumbe_group_details_html_page',
        'access arguments' => array('access ujumbe content'),
        'type' => MENU_CALLBACK,
    );
  }
  
}// End class

}// End namespace
