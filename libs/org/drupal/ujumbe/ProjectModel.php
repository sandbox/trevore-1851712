<?php
namespace org\drupal\ujumbe;
use \org\drupal\BaseNodeModel as BaseClass;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
{// Begin namespace

/**
 * Model class for Ujumbe Projects.
 */
class ProjectModel extends BaseClass {
  const BUNDLE_NAME = 'ujumbe_project';
  const BUNDLE_AS_URL = 'ujumbe-project';

  /**
   * @return \org\drupal\ujumbe\ProjectModel
   */
  static public function getNew() {
    return new ProjectModel();
  }
  
  /**
   * Gets the Drupal Bundle name for this content type.
   * Used by ancestor classes that do not have access to descendant consts.
   */
  public function getBundleName() {
    return self::BUNDLE_NAME;
  }
  
  /**
   * Gets our custom content type info for Drupal.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal custom content type info.
   */
  static public function getContentTypeInfo($t) {
    // Drupal 7 wants content type information as an associative array
    return array(
        'name' => $t('Ujumbe Project'),
        'base' => 'node_content',
        'description' => $t('The project, gateway and network information used to manage a project.'),
        'has_title' => '1',
        'title_label' => $t('Project Title'),
        'locked' => FALSE,
        'body_label' => $t('Description'),
    );
  }

  /**
   * @return Returns the installed fields info for our content type.
   */
  public function getFieldDefinitions() {
    // how to define checkbox fields: http://drupal.org/node/1339968
    return array(
        // defining "body" field in case its not there for some reason
        'body' => $this->getDefaultBodyFieldDefinition(),
        'ujumbe_project_status' => array(
            'field_name'  => 'ujumbe_project_status',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 20,
            ),
        ),
        'ujumbe_ushahidi_enabled' => array(
            'field_name'  => 'ujumbe_ushahidi_enabled',
            'type'        => 'list_boolean',
            'cardinality' => 1,
            'settings'    => array(
                'allowed_values' => drupal_map_assoc(range(0, 1)),
            ),
        ),
        'ujumbe_ushahidi_url' => array(
            'field_name'  => 'ujumbe_ushahidi_url',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 512,
            ),
        ),
        'ujumbe_auto_reply_enabled' => array(
            'field_name'  => 'ujumbe_auto_reply_enabled',
            'type'        => 'list_boolean',
            'cardinality' => 1,
            'settings'    => array(
                'allowed_values' => drupal_map_assoc(range(0, 1)),
            ),
        ),
        'ujumbe_auto_reply_text' => array(
            'field_name'  => 'ujumbe_auto_reply_text',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 160,
            ),
        ),
        'ujumbe_gateway_power' => array(
            'field_name'  => 'ujumbe_gateway_power',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 20,
            ),
        ),
        'ujumbe_gateway_network' => array(
            'field_name'  => 'ujumbe_gateway_network',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 20,
            ),
        ),
        'ujumbe_gateway_time' => array(
            'field_name'  => 'ujumbe_gateway_time',
            'type'        => 'date',
        ),
    );
    
    /* 
  // Exported field_base: 'field_mynewintlist'
  $field_bases['field_mynewintlist'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_mynewintlist',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Unknown',
        1 => 'Active',
        2 => 'Inactive',
        3 => 'Banned',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_mynewlist'
  $field_bases['field_mynewlist'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_mynewlist',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Active' => 'Active',
        'off' => 'inactive',
        'blah' => 'unknown',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );
     */
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getDbSchema()
   */
  public function getDbSchema($aFieldInfo) {
    /*
    $columns = array(
        'rgb' => array('type' => 'varchar', 'length' => 7, 'not null' => FALSE),
    );
    $indexes = array(
        'rgb' => array('rgb'),
    );
    return array(
        'columns' => $columns,
        'indexes' => $indexes,
    );
    */
    return array();
  }
  
  /**
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal field instance info for our content type.
   */
  public function getFieldInstances($t) {
    return array(
        'body' => $this->getBodyFieldInstance($t),
        'ujumbe_project_status' => array(
            'field_name'  => 'ujumbe_project_status',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Project Status'),
            'widget'      => array('type' => 'text_textfield',),
            //'description' => $t(''),
        ),
        'ujumbe_ushahidi_enabled' => array(
            'field_name'  => 'ujumbe_ushahidi_enabled',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Ushahidi Enabled'),
            'widget'      => array(
                'type' => 'options_onoff', 
                'settings' => array('display_label' => 1),
            ),
            //'description' => $t(''),
        ),
        'ujumbe_ushahidi_url' => array(
            'field_name'  => 'ujumbe_ushahidi_url',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Ushahidi URL'),
            'widget'      => array('type' => 'text_textfield',),
            'description' => $t('The FrontlineSMS URL for an Ushahidi project to push messages to.'),
        ),
        'ujumbe_auto_reply_enabled' => array(
            'field_name'  => 'ujumbe_auto_reply_enabled',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Auto Reply Enabled'),
            'widget'      => array(
                'type' => 'options_onoff', 
                'settings' => array('display_label' => 1),
                 
            ),
            //'description' => $t(''),
        ),
        'ujumbe_auto_reply_text' => array(
            'field_name'  => 'ujumbe_auto_reply_text',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Auto Reply Text'),
            'widget'      => array('type' => 'text_textfield',),
            //'description' => $t(''),
        ),
        'ujumbe_gateway_power' => array(
            'field_name'  => 'ujumbe_gateway_power',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Gateway Power Type'),
            'widget'      => array('type' => 'text_textfield',),
            //'description' => $t(''),
        ),
        'ujumbe_gateway_network' => array(
            'field_name'  => 'ujumbe_gateway_network',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Gateway Network Status'),
            'widget'      => array('type' => 'text_textfield',),
            //'description' => $t(''),
        ),
        'ujumbe_gateway_time' => array(
            'field_name'  => 'ujumbe_gateway_time',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Gateway Last Seen'),
            'settings' => array(
                'default_value' => 'now',
                'default_value2' => 'same',
                'default_value_code' => '',
                'default_value_code2' => '',
                'user_register_form' => FALSE,
            ),
            'widget'      => array(
                'settings' => array(
                    'increment' => 15,
                    'input_format' => 'Y-m-d H:i:s',
                    'input_format_custom' => '',
                    'label_position' => 'above',
                    'text_parts' => array(),
                    'year_range' => '-3:+3',
                ),
                'type' => 'date_text',
            ),
            //'description' => $t(''),
        ),
    );
    /*
  // Exported field_instance: 'node-ujumbe_project-field_mynewintlist'
  $field_instances['node-ujumbe_project-field_mynewintlist'] = array(
    'bundle' => 'ujumbe_project',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'my help text for int list field',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mynewintlist',
    'label' => 'MyNewIntList',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-ujumbe_project-field_mynewlist'
  $field_instances['node-ujumbe_project-field_mynewlist'] = array(
    'bundle' => 'ujumbe_project',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mynewlist',
    'label' => 'MyNewList',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Description');
  t('MyNewIntList');
  t('MyNewList');
  t('my help text for int list field');
     */
    /* TODO using the above commented code as a template, make sure we put translatable
     * strings at the end of the method here like they do for our $t() strings.
     */
  }
  
  /**
   * Get all of the project gateways in the database
   *
   * @param int $aPageNum
   *   (optional) The page number to get the projects for
   *
   * @return
   *   A Drupal loaded entity object array of Node object results.
   */
  public function getProjects($aPageNum = -1) {
    $theNodeQuery = $this->getNewNodeQuery($aPageNum)->propertyOrderBy('nid');
    return NodeUtils::executeNodeQueryAndLoadResults($theNodeQuery);
  }
  
  /**
   * Get all of the project gateways with issues
   *
   * @param int $aPageNum
   *   (optional) The page number to get the projects for
   *
   * @return
   *   A Drupal loaded entity object array of Node object results.
   */
  public function getActiveProjects($aPageNum = -1) {
    $theNodeQuery = $this->getNewNodeQuery($aPageNum)->propertyOrderBy('nid');
    // Uncomment the following line to only have projects with issues show up in the dashboard
    //$query -> condition(db_or()->condition('p.gateway_network', '%green%', 'NOT LIKE')->condition('p.gateway_power', '%green%', 'NOT LIKE'));
    //PROBLEM: EFQ does not support OR conditions.  Workarounds described:
    //  http://www.phase2technology.com/blog/or-queries-with-entityfieldquery/
    //  http://drupal.stackexchange.com/questions/14499/using-or-with-entityfieldquery
    $theNodeQuery->fieldCondition('project_status','value', '%inactive%', 'NOT LIKE');
    return NodeUtils::executeNodeQueryAndLoadResults($theNodeQuery);
  }
  
  /**
   * Return the total number of projects
   *
   * @return
   *   The number of project gateways.
   */
  public function getCount() {
    return $this->getNewNodeQuery()->count()->execute();
  }

  /**
   * Updates the status of a gateway on a project
   * @param int $aProjectId - The ID of the project
   * @param string $aGatewayPower - Text describing what kind of power source.
   * @param string $aGatewayNetwork - Text describing the kind of network connection.
   */
  public function updateGatewayStatus($aProjectId, $aGatewayPower = "Unknown", $aGatewayNetwork = "Unknown") {
    $theProject = $this->getNode($aProjectId);
    $theProject->ujumbe_gateway_power->set($aGatewayPower);
    $theProject->ujumbe_gateway_network->set($aGatewayNetwork);
    $theProject->ujumbe_project_status->set('Active');
    return $theProject->save();
  }
  
  /**
   * Gets the automatic reply for a message
   *
   * @param $project_id
   *   The ID of the project
   *
   * @param $incoming_text
   *   The incoming text to reply to
   *
   * @return
   *   The text of the automatic reply
   */
  public function getAutoReply($project_id, $incoming_text = NULL) {
  	$max_length = variable_get('ujumbe_max_length', 160);
  	$theProject = $this->getNode($project_id);
  	if ($theProject->ujumbe_auto_reply_enabled->value() == 1) {
  	  $project_reply = $theProject->ujumbe_auto_reply_text->value();
    	// If the auto-reply text has a %m replace that with the incoming message
    	if (preg_match("/%m/", $project_reply)) {
    		$project_reply_length = strlen($project_reply);
    		$incoming_text_length = strlen($incoming_text);
    		// Need to add one to the quote length since the %m will be replaced.
    		$quote_length = $max_length - $project_reply_length + 1;
    
    		// Truncate the incoming text to make sure the reply does not go over
    		// the defined max length of message
    		if ($quote_length < $incoming_text_length) {
    			$incoming_text = substr($incoming_text, 0, $quote_length);
    		} 
    		else {
    			// Or no quoted text in the reply
    			$reply = $project_reply;
    		}
    
    		// The 1 makes sure that only the first instance of %m is replaced
    		// That way it's easier to make sure the message does not go over the
    		// configured max length
    		$reply = preg_replace("/%m/", "$incoming_text", $project_reply, 1);
    	  return $reply;
    	}
    	else {
    	  return $project_reply;
    	}
  	}
  }
  
}// End class

}// End namespace
