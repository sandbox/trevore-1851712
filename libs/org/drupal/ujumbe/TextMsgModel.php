<?php
namespace org\drupal\ujumbe;
use \org\drupal\BaseNodeModel as BaseClass;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\RespondentModel as UjumbeRespondents;
{// Begin namespace

class TextMsgModel extends BaseClass {
  const BUNDLE_NAME = 'ujumbe_message';
  const BUNDLE_AS_URL = 'ujumbe-message';
  
  const MSG_DIRECTION_INCOMING = 0;
  const MSG_DIRECTION_OUTGOING = 1;
  
  const XMIT_STATUS_RECEIVED = 0;
  const XMIT_STATUS_SENT = 1;
  const XMIT_STATUS_OUTGOING = 2;
  const XMIT_STATUS_OUTPENDING = 3; // Sent to gateway, unconfirmed SENT

  /**
   * The project id to return messages for. If NULL, return messages for all projects.
   * @var integer
   */
  public $mProjectId = null;

  /**
   * The responder id to return messages for. If NULL, return messages for all responders.
   * @var integer
   */
  public $mResponderId = null;

  /**
   * @return \org\drupal\ujumbe\TextMsgModel
   */
  static public function getNew() {
    return new TextMsgModel();
  }
  
  /**
   * @param int $aProjectId
   *     If Project NID is not empty, results will get auto-filtered.
   *
   * @return \org\drupal\ujumbe\TextMsgModel
   */
  static public function getNewForProject($aProjectId=null) {
    $theObj = new TextMsgModel();
    $theObj->mProjectId = $aProjectId;
    return $theObj;
  }
  
  /**
   * @param int $aResponderId
   *     If Responder NID is used, results will get auto-filtered.
   *      
   * @return \org\drupal\ujumbe\TextMsgModel
   */
  static public function getNewForResponder($aResponderId=null) {
    $theObj = new TextMsgModel();
    $theObj->mResponderId = $aResponderId;
    return $theObj;
  }
  
  /**
   * Gets the Drupal Bundle name for this content type.
   * Used by ancestor classes that do not have access to descendant consts.
   */
  public function getBundleName() {
    return self::BUNDLE_NAME;
  }
  
  /**
   * Gets our custom content type info for Drupal.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal custom content type info.
   */
  static public function getContentTypeInfo($t) {
    // Drupal 7 wants content type information as an associative array
    return array(
        'name' => $t('Ujumbe Message'),
        'base' => 'node_content',
        'description' => $t('Incoming and outgoing Ujumbe messages.'),
        'has_title' => '1',
        'title_label' => $t('Title'),
        'locked' => FALSE,
        'body_label' => $t('Notes'),
    );
  }
  
  /**
   * @return Returns the installed fields information.
   */
  public function getFieldDefinitions() {
    return array(
        // defining "body" field in case its not there for some reason
        'body' => $this->getDefaultBodyFieldDefinition(),
        'ujumbe_message_direction' => array(
            'field_name'  => 'ujumbe_message_direction',
            'type'        => 'list_boolean',
            'cardinality' => 1,
            'settings'    => array( //0 means incomming
                'allowed_values' => array(
                    0 => 'Incoming', 
                    1 => 'Outgoing',
                ),
            ),
        ),
        'ujumbe_message' => array(
            'field_name'  => 'ujumbe_message',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 160,
            ),
        ),
        'ujumbe_translation' => array(
            'field_name'  => 'ujumbe_translation',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 255,
            ),
        ),
        'ujumbe_project_nid' => $this->getNodeIdFieldDefinition('ujumbe_project_nid', 
            UjumbeProjects::BUNDLE_NAME, TRUE),
        'ujumbe_responder_nid' => $this->getNodeIdFieldDefinition('ujumbe_responder_nid', 
            UjumbeRespondents::BUNDLE_NAME, FALSE),
        'ujumbe_source_id' => array(
            'field_name'  => 'ujumbe_source_id',
            'type'        => 'number_integer',
        ),
        'ujumbe_external_identifier' => array(
            'field_name'  => 'ujumbe_external_identifier',
            'type'        => 'text',
            'settings'    => array(
                'max_length' => 255,
            ),
        ),
        'ujumbe_archived' => array(
            'field_name'  => 'ujumbe_archived',
            'type'        => 'list_boolean',
            'cardinality' => 1,
            'settings'    => array(
                'allowed_values' => array(
                    0 => t('Active'), 
                    1 => t('Archived'),
                ),
            ),
        ),
        'ujumbe_xmit_status' => array(
            'field_name'  => 'ujumbe_xmit_status',
            'type'        => 'list_integer',
            'cardinality' => 1,
            'settings'    => array(
                'allowed_values' => array(
                    self::XMIT_STATUS_RECEIVED => t('Received'),
                    self::XMIT_STATUS_SENT => t('Sent'),
                    self::XMIT_STATUS_OUTGOING => t('Outgoing'),
                    self::XMIT_STATUS_OUTPENDING => t('Sending'),
                ),
                'allowed_values_function' => '',
            ),
           'indexes' => array(
               'value' => array(
                   0 => 'value',
               ),
           ),
        ),
    );
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getDbSchema()
   */
  public function getDbSchema($aFieldInfo) {
    return array();
  }
  
  /**
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal field instance info for our content type.
   */
  public function getFieldInstances($t) {
    return array(
        'ujumbe_message_direction' => array(
            'field_name'  => 'ujumbe_message_direction',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Outgoing'),
            'display' => array(
                'default' => array(
                    'weight' => 2,
                ),
            ),
            'widget'      => array(
                'type' => 'options_onoff', 
                'settings' => array('display_label' => 1),
            ),
        ),
        'ujumbe_message' => array(
            'field_name'  => 'ujumbe_message',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Message'),
            'display' => array(
                'default' => array(
                    'weight' => 3,
                ),
            ),
            'widget'      => array('type' => 'text_textfield',),
        ),
        'ujumbe_translation' => array(
            'field_name'  => 'ujumbe_translation',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Translation'),
            'display' => array(
                'default' => array(
                    'weight' => 4,
                ),
            ),
            'widget'      => array('type' => 'text_textfield',),
        ),
        'ujumbe_project_nid' => $this->getNodeIdFieldInstance('ujumbe_project_nid', array(
            'label' => $t('Project Node'),
            'widget' => array(
                '#readonly' => TRUE,
            ),
        )),
        'ujumbe_responder_nid' => $this->getNodeIdFieldInstance('ujumbe_responder_nid', array(
            'label' => $t('Responder Node'),
            'widget' => array(
                '#readonly' => TRUE,
            ),
        )),
        'ujumbe_source_id' => array(
            'field_name'  => 'ujumbe_source_id',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Source ID'),
            'display' => array(
                'default' => array(
                    'weight' => 7,
                ),
            ),
            'widget'      => array('type' => 'number',),
        ),
        'ujumbe_external_identifier' => array(
            'field_name'  => 'ujumbe_external_identifier',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Source'),
            'display' => array(
                'default' => array(
                    'weight' => 8,
                ),
            ),
            'widget'      => array('type' => 'text_textfield',),
        ),
        'ujumbe_archived' => array(
            'field_name'  => 'ujumbe_archived',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Archived'),
            'display' => array(
                'default' => array(
                    'weight' => 9,
                ),
            ),
            'widget'      => array(
                'type' => 'options_onoff', 
                'settings' => array(
                    'display_label' => 1,
                ),
            ),
        ),
        'ujumbe_xmit_status' => array(
            'field_name'  => 'ujumbe_xmit_status',
            'entity_type' => 'node',
            'bundle'      => self::BUNDLE_NAME,
            'label'       => $t('Transmit Status'),
            'default_value' => array(
                0 => array(
                    'value' => 0,
                ),
            ),
            'display' => array(
                'default' => array(
                    'weight' => 10,
                ),
            ),
            'widget' => array(
                'type' => 'options_select',
            ),
        ),
        'body' => $this->getBodyFieldInstance($t, array(
            'label' => $t('Notes'),
            'display' => array(
                'default' => array(
                    'weight' => 16,
                ),
            ),
        )),
    );
  }

  /**
   * @see \org\drupal\BaseNodeModel::getNewNodeQuery()
   */
  public function getNewNodeQuery($aPageNum = -1) {
    $theNodeQuery = parent::getNewNodeQuery($aPageNum);
    if ($this->mProjectId) {
      $theNodeQuery->fieldCondition('ujumbe_project_nid', 'value', $this->mProjectId, '=');
    }
    if ($this->mResponderId) {
      $theNodeQuery->fieldCondition('ujumbe_responder_nid', 'value', $this->mResponderId, '=');
    }
    return $theNodeQuery;
  }

  /**
   * Get a new Entity object filled with required fields.
   * @param string $aPhone - where the call came from
   * @param string $aMsgText - text of the message.
   * @param boolean $bIncoming - message is being received if TRUE
   * @return Returns a new \EntityMetadataWrapper.
   */
  public function createNewEntity($aPhone, $aMsgText, $bIncoming = TRUE) {
    $theNewEntity = NodeUtils::createNode(self::BUNDLE_NAME);
   
    $theNewEntity->title->set($this->sanitizePhoneNumber($aPhone) . '_' . $aMsgText);
    if ($bIncoming) {
      $theNewEntity->ujumbe_message_direction->set(0);
      $theNewEntity->ujumbe_xmit_status->set(self::XMIT_STATUS_RECEIVED);
    }
    else {
      $theNewEntity->ujumbe_message_direction->set(1);
      $theNewEntity->ujumbe_xmit_status->set(self::XMIT_STATUS_OUTGOING);
    }
    $theNewEntity->ujumbe_message->set($aMsgText);
    $theNewEntity->ujumbe_archived->set(0);
    return $theNewEntity;
  }

  /**
   * Insert a new text message into the db.
   * @param string $aPhone - where the call came from
   * @param string $aMsgText - text of the message.
   * @param boolean $bIncoming - (optional) message is being received if TRUE
   * @param int $aResponderId - (optional) the responder nid
   * @param int $aProjectId - (optional) the project nid
   * //@param int $aSourceId - (optional) idk
   * @param int $aIdentifier - (optional) idk
   * @param string $aTranslation - (optional) message translated into English
   * @param string $aNotes - (optional) notes about the message
   * @param boolean $bArchived - (optional) is message archived
   * @return Returns the ID of the new record.
   */
  public function insertMessage($aPhone, $aMsgText, $bIncoming = TRUE, $aResponderId = 0, $aProjectId = 0, 
      $aIdentifier = NULL, $aTranslation = NULL, $aNotes = NULL, $bArchived = FALSE) {
    $theNewMsg = $this->createNewEntity($aPhone, $aMsgText, $bIncoming);
    $theNewMsg->ujumbe_responder_nid->set($aResponderId);
    $theNewMsg->ujumbe_project_nid->set($aProjectId);
    //$theNewMsg->ujumbe_source_id->set($aSourceId);
    $theNewMsg->ujumbe_external_identifier->set($aIdentifier);
    $theNewMsg->ujumbe_translation->set($aTranslation);
    $theNewMsg->body->set($aNotes);
    if ($bArchived) {
      $theNewMsg->status->set(NODE_NOT_PUBLISHED);
      $theNewMsg->ujumbe_archived->set(1);
    }
    else {
      $theNewMsg->ujumbe_archived->set(0);
    }
    $theNewMsg->save();
    if ($bIncoming) {
      //TODO make sure Ushahidi integration works
      //_ujumbe_ushahidi_process($aProjectId, $aMsgText, $this->sanitizePhoneNumber($aPhone));
    }
    return $theNewMsg->getIdentifier();
  }
    
  /**
   * Check to see if a message should recieve an automatic reply.
   *
   * @param $aPhone - The sanitized number to be checked
   *
   * @return Return true if message should get an automatic reply.
   */
  public function shouldAutoReply($aPhone) {
    $thePhoneNum = $this->sanitizePhoneNumber($aPhone);
    $filter_min = variable_get('ujumbe_filter_minimum_length', 0);
    if (($filter_min > 0) AND (strlen($thePhoneNum) < $filter_min)) { 
      return FALSE;
    }
    
    $filter_alpha = variable_get('ujumbe_filter_alpha', 0);
    if ($filter_alpha == 1 AND !is_numeric($thePhoneNum)) { 
      return FALSE;
    }
  
    return TRUE;
  }

  /**
   * Update the status of an outgoing message.
   * This is primaraly used to indicate when a message has been sent so it 
   * does not get sent multiple times.
   *
   * @param int $aMsgId - The NID of the message to be updated
   * @param int $aMsgStatus - One of the UjumbeMessage constants.
   */
  public function updateMsgStatus($aMsgId, $aMsgStatus) {
    $theNode = node_load($aMsgId);
    $theWrapper = entity_metadata_wrapper('node', $theNode);
    //$theNode->ujumbe_xmit_status[$theNode->language][0]['value'] = $aMsgStatus;
    $theWrapper->ujumbe_xmit_status->set($aMsgStatus);
    
    // Entity API cannot handle revisions yet, fall back to using $theNode
    // Make this change a new revision
    $theNode->revision = 1;
    $theNode->log = 'This node was programmatically updated to ' . self::$XMIT_STATUS[$aMsgStatus] . ' at ' . date('c');
    
    node_save($theNode);
  }
  
  /**
   * Get the messages for the given project, person and page number
   * 
   * @param int $page
   *   (optional) The page number to return messages for. If ommited then return all messages
   * 
   * @param int $person_id
   *   (optional) The person id to return messages for. If omitted return messages for all people
   *
   * @return
   *   The mysql result of the query
   */
  function getMessages($page = -1, $person_id = "", $inout = "") {
    $max_range = $this->getMaxPerPage();
    /*
example!
    $query = new EntityFieldQuery();
    
    $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'article')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_news_types', 'value', 'spotlight', '=')
    ->fieldCondition('field_photo', 'fid', 'NULL', '!=')
    ->fieldCondition('field_faculty_tag', 'tid', $value)
    ->fieldCondition('field_news_publishdate', 'value', $year. '%', 'like')
    ->range(0, 10)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
    
    $result = $query->execute();
    
    if (isset($result['node'])) {
      $news_items_nids = array_keys($result['node']);
      $news_items = entity_load('node', $news_items_nids);
    }
....    
    */
    $query = db_select('ist_message', 'm');
    $query -> fields('m', array('message_id', 'project_id', 'person_id', 'message', 'translation', 'notes', 'source_id', 'source', 'source_table', 'timestamp', 'type'));
    $query -> orderBy('m.message_id', 'DESC');
    $query -> condition('m.archived', '0', '=');
    if ($this->mProjectId) {
      $query -> condition('m.project_id', $this->mProjectId, '=');
    }
    if($person_id) {
      $query -> condition('m.person_id', $person_id, '=');
    }
    if($inout == "Incoming") {
      $query -> condition('m.type', 'Incoming', '=');
    } else if($inout == "Outgoing") {
      $query -> condition('m.type', 'Outgoing', '=');
    }
    if ($page >=0) {
      $query -> range($page * $max_range,$max_range);
    }
    $result = $query -> execute();
    return $result;
  }
  
  /**
   * Return the total number of messages for the given person/project ID
   *
   * @param $person_id 
   *   (optional) The ID of the person to return the count for
   *
   * @param $inout
   *   (optional) "Incoming", "Outgoing" filters accordingly
   *   
   * @return
   *   The number of messages for the given person/project
   */  
  function getMessageCount($person_id = "", $inout = "") {
    $query = db_select('ist_message', 'm');
    $query -> fields('m', array('message_id'));
    if ($this->mProjectId) {
      $query -> condition('m.project_id', $this->mProjectId, '=');
    }
    if($person_id) {
      $query -> condition('m.person_id', $person_id, '=');
    }
    if($inout == "Incoming") {
      $query -> condition('m.type', 'Incoming', '=');
    } else if($inout == "Outgoing") {
      $query -> condition('m.type', 'Outgoing', '=');
    }
    $result = $query -> execute();
    $count = $result -> rowCount();
    return $count;
  }
  
  /**
   * Sends a message
   * 
   * @param $project_id
   *   The ID of the project
   * @param $number
   *   The phone number of the recipent.
   * @param $message
   *   The message to send
   *
  */
  function _ujumbe_send($numbers, $message) {
    $numbers_array = preg_split("/\,/", $numbers);
    foreach ($numbers_array as $number) {
      $normal_number = _ujumbe_normal_number($number);
  
      $query = 'INSERT INTO {ist_envaya_outgoing} SET project_id= :project_id, number = :number, message = :message';
      $values= array(':project_id'=>$this->mProjectId, ':number'=>$normal_number, ':message'=> $message);
      db_query($query,$values);
  
      $query2 = "SELECT LAST_INSERT_ID() AS id";
      $result = db_query($query2);
      $row = $result->fetchAssoc();
      $message_id = $row['id'];
      _ujumbe_insert_person($normal_number);
      _ujumbe_insert_message(
            $this->mProjectId, 
            $normal_number,
            $message, 
            $message_id, 
            "Envaya", 
            "ist_envaya_outgoing",
            "Outgoing"                                                                                                
          );
    }
  
    drupal_set_message(t("Message sent."));
  } //function _ujumbe_send
  
}// End class

}// End namespace
