<?php
namespace org\drupal\ujumbe;
use \stdClass as BaseClass;
use \Exception;
use \DateTime;
use \EnvayaSMS;
use \EnvayaSMS_ActionRequest;
use \EnvayaSMS_OutgoingMessage;
use \EnvayaSMS_Event_Send;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\TextMsgModel as UjumbeMessages;
use \org\drupal\ujumbe\RespondentModel as UjumbeRespondents;
{// Begin namespace

/**
 * Bridge class between phones running EnvayaSMS and Ujumbe.
 * EnvayaSMS is a required library for this class.
 * It should be located in sites/all/libraries/EnvayaSMS
 *
 * It can be downloaded with the following command from sites/all/libraries.
 * git clone https://github.com/youngj/EnvayaSMS.git
 *
 * For more info on EnvayaSMS go to {@link http://sms.envaya.org}
 */
class EnvayaSMSBridge extends BaseClass {
  const SETTING_PASSWORD = 'ujumbe_envaya_password';
  const DEFAULT_PASSWORD = 'rosebud';

  /**
   * May return NULL if EnvayaSMS library does not exist.
   * @return \org\drupal\ujumbe\EnvayaSMSBridge
   */
  static public function getNew() {
    if (($library = libraries_load('EnvayaSMS')) && !empty($library['installed'])) {
      return new EnvayaSMSBridge();
    }
  }

  /**
   * @return Return TRUE if library exists.
   */
  static public function libraryExists() {
    if (($library = libraries_detect('EnvayaSMS')) && !empty($library['installed'])) {
      // The library is installed. Awesome!
      $theErrMsg = t('@name found!',array('@name' => $library['name']));
      drupal_set_message($theErrMsg,'status');
      return TRUE;
    }
    else {
      // Something went wrong. :(
      $theErrMsg = t('@name not found in sites/all/libraries', array('@name' => $library['name']));
      drupal_set_message($theErrMsg,'error');
      watchdog('ujumbe',$theErrMsg, array(), WATCHDOG_ERROR,$library['vendor url']);
    }
    return FALSE;
  }

  /**
   * @return Returns the Ujumbe Settings defined password.
   */
  static public function getDefinedPassword() {
    return variable_get(self::SETTING_PASSWORD, self::DEFAULT_PASSWORD);
  }

  /**
   * Modify the parameter to contain our hook_menu() info.
   * @param array $aMenuInfos - module menu info.
   */
  static public function buildMenuInfo(array &$aMenuInfos) {
    $aMenuInfos['ujumbe/envaya'] = array(
      'page callback' => __NAMESPACE__ . '\EnvayaSMSBridge::onPageCallback',
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    );
    $aMenuInfos['envaya'] = array(
      'page callback' => __NAMESPACE__ . '\EnvayaSMSBridge::onPageCallback',
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    );
  }

  /**
   * Page callback method used in Drupal's menu definition
   */
  static public function onPageCallback($aProjectId = NULL, $aUser = NULL) {
    $theEnvayaBridge = self::getNew();
    // ensure we do not crash if the library is not found
    if (!empty($theEnvayaBridge)) {

      // if URL was not followed by a project id "menu_url/%", check for ?id= or ?project=
      $theProjectId = NULL;
      if (!empty($aProjectId)) {
        $theProjectId = $aProjectId;
      }
      else if (!empty($_GET['id'])) {
        $theProjectId = $_GET['id'];
      }
      else if (!empty($_GET['project'])) {
        $theProjectId = $_GET['project'];
      }

      // Entity API fails if no user is "logged in" which would be true if running this code
      //   this is because we removed all rights to view/modify the data from unauthenticated users
      global $user;
      $userName = NULL;
      $uid = 1; //super user created on website install is the default user account for all incoming msgs
      // "menu_url/%project_id/%user" where %user is either uid or name.
      if (!empty($aUser)) {
        if (is_numeric($aUser)) {
          $tempUser = user_load($aUser, FALSE); //false means use cache if available
          $userName = $tempUser->name;
        }
        else {
          $userName = $aUser;
        }
      }
      // else check for ?uid= or ?name=
      else if (!empty($_GET['uid'])) {
      	$tempUser = user_load($_GET['uid'], FALSE); //false means use cache if available
      	$userName = $tempUser->name;
      }
      else if (!empty($_GET['name'])) {
      	$userName = $_GET['name'];
      }
      // able to use an alternative User ID or NAME, only as long as that user has same pw as in settings.
      //TODO improve this area?
      $uid = (empty($userName)) ? $uid : user_authenticate($userName, self::getDefinedPassword());
      if ($uid) {
        $user = user_load($uid, TRUE);
        drupal_session_regenerate();
      }

      return $theEnvayaBridge->handleRequest($theProjectId);
    }
  }

  /**
   * This is the main function to interface with phones running EnvayaSMS
   */
  public function handleRequest($aProjectId) {
    $request = EnvayaSMS::get_request();

    // check for REST request, if just a typed in URL, show Drupal's 404 page
    if (empty($request) || !($request instanceof EnvayaSMS_ActionRequest)) {
      return drupal_not_found();
    }

    header('Content-Type: ' . $request->get_response_type());

    if (empty($aProjectId)) {
      header('HTTP/1.1 404 Not Found');
      echo $request->render_error_response(t('Unknown project.'));
      return;
    }

    if (!$request->is_validated(self::getDefinedPassword())) {
      watchdog('EnvayaSMS', 'Invalid password for ID = :id', array(':id' => $aProjectId), WATCHDOG_WARNING);
      header('HTTP/1.1 403 Forbidden');
      echo $request->render_error_response(t('Invalid password'));
      return;
    }

    $action = $request->get_action();
    //watchdog('EnvayaSMS', $action->type, array(), WATCHDOG_NOTICE);
    switch ($action->type) {
      case EnvayaSMS::ACTION_INCOMING:
        if ($action->message_type != EnvayaSMS::MESSAGE_TYPE_CALL) {
          return $this->handleActionIncomingText($aProjectId, $request, $action);
        }
        else {
          return $this->handleActionIncomingCall($aProjectId, $request, $action);
        }
      case EnvayaSMS::ACTION_OUTGOING:
        return $this->handleActionOutgoing($aProjectId, $request);
      case EnvayaSMS::ACTION_SEND_STATUS:
        return $this->handleActionSendStatus($action);
      case EnvayaSMS::ACTION_DEVICE_STATUS:
        watchdog('EnvayaSMS', 'device_status = :status', array(':status' => $action->status), WATCHDOG_INFO);
        echo $request->render_response();
        return;
      case EnvayaSMS::ACTION_FORWARD_SENT:
        return $this->handleActionForwardSent($aProjectId, $action);
      case EnvayaSMS::ACTION_TEST:
        echo $request->render_response();
        return;
      default:
        header('HTTP/1.1 404 Not Found');
        echo $request->render_error_response(t('The server does not support the requested action.'));
    }
  }

  /**
   * Handle the EnvayaSMS action: Incoming SMS/MMS message.
   * @param int $aProjectId - the Project ID.
   * @param object $request - the EnvayaSMS object.
   * @param object $action - the Action object.
   */
  protected function handleActionIncomingText($aProjectId, $request, $action) {
    $dbMessages = UjumbeMessages::getNew();
    $fromPhone = $dbMessages->sanitizePhoneNumber($action->from);

    $bShouldAutoReply = $dbMessages->shouldAutoReply($fromPhone);
    $archive_set = variable_get('ujumbe_filter_archive', 0);
    $bArchived = FALSE;
    if (!$bShouldAutoReply AND $archive_set == 1) {
      $bArchived = TRUE;
    }

    $theMessage = trim($action->message);
    /* do we have to worry about MMS text read in the parts to create a whole message?
    if ($action->message_type == EnvayaSMS::MESSAGE_TYPE_MMS) {
    	foreach ($action->mms_parts as $mms_part) {
    		if ($mms_part->type=='text/plain') {
    		  //$theMessage .= $mms_part->tmp_name;
    		}
    	}
    }
    */

    $theResponder = UjumbeRespondents::getNew()->insertRespondent($fromPhone);

    $theNewMsg = $dbMessages->createNewEntity($fromPhone, $theMessage, TRUE);
    $theNewMsg->ujumbe_responder_nid->set($theResponder);
    $theNewMsg->ujumbe_project_nid->set($aProjectId);
    //$theNewMsg->ujumbe_source_id->set($aSourceId);
    //$theNewMsg->ujumbe_external_identifier->set('Envaya');
    //$theNewMsg->ujumbe_translation->set($aTranslation);
    //$theNewMsg->body->set($aNotes);
    if ($bArchived) {
    	$theNewMsg->status->set(NODE_NOT_PUBLISHED);
    	$theNewMsg->ujumbe_archived->set(1);
    }
    else {
    	$theNewMsg->ujumbe_archived->set(0);
    }
    // Envaya sends us "microtime" but we can only handle to the nearest second.
    $theReceivedDate = DateTime::createFromFormat('U',round($action->timestamp/1000));
    // set publish date to when msg received at gateway device
    $theNewMsg->created->set($theReceivedDate->getTimestamp());

    /*TODO MMS parts stored in a File field (need to add a file field type in messages)
    if ($action->message_type == EnvayaSMS::MESSAGE_TYPE_MMS) {
      foreach ($action->mms_parts as $mms_part) {
        $ext_map = array('image/jpeg' => '.jpg', 'image/gif' => '.gif', 'text/plain' => '.txt', 'application/smil' => '.smil');
        $ext = @$ext_map[$mms_part->type] ?: ".bin";

        $filename = "envaya/mms_parts/" . uniqid('mms') . $ext;

        copy($mms_part->tmp_name, dirname(__DIR__)."/$filename");
        _ujumbe_write_log("Envaya", " mms part type {$mms_part->type} saved to {$filename}");
      }
    }
    */

    $theNewMsg->save();

   	//TODO make sure Ushahidi integration works  (should archived be processed too?)
   	//_ujumbe_ushahidi_process($aProjectId, $aMsgText, $fromPhone);

    $auto_reply = UjumbeProjects::getNew()->getAutoReply($aProjectId, $theMessage);
    if (!empty($auto_reply) AND $bShouldAutoReply) {
      $reply = new EnvayaSMS_OutgoingMessage();
      $reply->message = $auto_reply;
      echo $request->render_response(
          array( new EnvayaSMS_Event_Send(array($reply)) )
      );

      $theNewMsg = $dbMessages->createNewEntity($fromPhone, $auto_reply, FALSE);
      $theNewMsg->ujumbe_responder_nid->set($theResponder);
      $theNewMsg->ujumbe_project_nid->set($aProjectId);
      //$theNewMsg->ujumbe_source_id->set($aSourceId);
      $theNewMsg->ujumbe_external_identifier->set('Envaya');
      $theNewMsg->save();
    }
    else {
      echo $request->render_response();
    }
  }

  /**
   * Handle the EnvayaSMS action: Incoming call.
   * @param int $aProjectId - the Project ID.
   * @param object $request - the EnvayaSMS object.
   * @param object $action - the Action object.
   */
  protected function handleActionIncomingCall($aProjectId, $request, $action) {
    $theRespondentId = UjumbeRespondents::getNew()->insertRespondent($action->from);
    UjumbeCalls::getNew()->insertCall($action->from, $theRespondentId, $aProjectId);
  }

  /**
   * Handle the EnvayaSMS action: Outgoing SMS message.
   * @param int $aProjectId - the Project ID.
   * @param object $request - the EnvayaSMS object.
   */
  protected function handleActionOutgoing($aProjectId, $request) {
    $this->setPhoneStatus($request->battery, $request->power, $request->network, $request);
    //header('HTTP/1.1 403 Forbidden'); echo $request->render_error_response('blah action outgoing 2'); return;

    $messages = array();
    $theNodeList = $this->getOutgoingMessages($aProjectId);
    foreach ((array)$theNodeList as $nid) {
      $theMsgNode = NodeUtils::getNode($nid);
      $sms = new EnvayaSMS_OutgoingMessage();
      $sms->id = $theMsgNode->getIdentifier();
      $sms->to = $theMsgNode->title->value(); //title is the phone number
      //TODO actually, no its not, we need to load the respondent and grab ITS title
      $sms->message = $theMsgNode->ujumbe_message->value();
      $messages[] = $sms;
    }

    $events = array();
    if ($messages) {
      $events[] = new EnvayaSMS_Event_Send($messages);
    }

    echo $request->render_response($events);
  }

  /**
   * Handle the EnvayaSMS action: Send Status.
   * @param object $action - the Action object.
   */
  protected function handleActionSendStatus($action) {
    $id = $action->id;  //TODO is this the same ID as what's in Drupal??? RTF++
    $status = $action->status;
    $error = $action->error;
    watchdog('EnvayaSMS', "Status message $id status: $status error: $error", array(), WATCHDOG_INFO);
    switch ($action->status) {
      //TODO fix this stuff so that if a failure previously occured, we email an "all is OK now" message
      case EnvayaSMS::STATUS_QUEUED:
        UjumbeMessages::getNew()->updateMsgStatus($id, UjumbeMessages::XMIT_STATUS_OUTPENDING);
        break;
      case EnvayaSMS::STATUS_SENT:
        UjumbeMessages::getNew()->updateMsgStatus($id, UjumbeMessages::XMIT_STATUS_SENT);
        break;
      case EnvayaSMS::STATUS_FAILED:
      case EnvayaSMS::STATUS_CANCELLED:
        global $base_url;
        $subject = "Ujumbe Message sending problem: $status";
        $message = "The gateway returned an irregular status response while trying to send a message. \n";
        $message .= "Envaya Message ID: $id\n";
        $message .= "Status: $status\n";
        $message .= "Error: $error\n";
        $message .= " \nURL: " . $base_url;
        watchdog('EnvayaSMS',$message, array(), WATCHDOG_ERROR);
        _ujumbe_notice_mail($subject, $message);
    }
    echo $request->render_response();
  }

  /**
   * Handle the EnvayaSMS action: Forward Sent message.
   * @param int $aProjectId - the Project ID.
   * @param object $action - the Action object.
   */
  protected function handleActionForwardSent($aProjectId, $action) {
    watchdog('EnvayaSMS','Forward Sent: :to :msg', array(':to' => $action->to, ':msg' => $action->message), WATCHDOG_INFO);
    $theRespondentId = UjumbeRespondents::getNew()->insertRespondent($action->from);
    $dbMsgs = UjumbeMessages::getNew();
    $dbMsgs->insertMessage($action->to, $action->message, FALSE, $theRespondentId, $aProjectId, 'Envaya');
    echo $request->render_response();
  }

  /**
   * Determins the status of the phone and updates a project with that status
   * @param int $aProjectId - the Project ID.
   */
  protected function setPhoneStatus($aProjectId, $battery, $power, $network, $request) {
  	// If it is on battery go yellow and say so. If battery is less then 50% go red
    if ($power == 0 ) {
      if ($battery > 50) {
        $power_color = "yellow";
        $power_status= "Battery ($battery%)";
      }
      else {
        $power_color = "red";
        $power_status= "Low Battery ($battery%)";
      }
    }
    else {
      $power_color = "green";
      $power_status = "Charger";
    }
    $power_html = "<font color='$power_color'>Power: $power_status</font>";

    // If it's on WIFI green, otherwise yellow. If there is no network, red, but we won't know here.
    if($network == "WIFI") {
      $network_color = "green";
    }
    else {
      $network_color = "yellow";
    }
    $network_html = "<font color='$network_color'>Network: $network</font>";
    try {
      UjumbeProjects::getNew()->updateGatewayStatus($aProjectId, $power_status, $network, $request);
    } catch (Exception $e) {
      $request->render_error_response($e->getMessage());
    }
  }

  /**
   * Get's the outgoing messages for the given project ID
   * @param int $aProjectId - get msgs from this project ID.
   * @return array Returns an array of node ids.
   */
  protected function getOutgoingMessages($aProjectId) {
    $dbMsgs = UjumbeMessages::getNew($aProjectId);
    $theNodeQuery = $dbMsgs->getNewNodeQuery()->propertyOrderBy('nid');
    $theNodeQuery->fieldCondition('ujumbe_message_direction', 'value', UjumbeMessages::MSG_DIRECTION_OUTGOING, '=');
    $theNodeQuery->fieldCondition('ujumbe_xmit_status', 'value', UjumbeMessages::XMIT_STATUS_OUTGOING, '=');
    //$theNodeQuery->addMetaData('account', user_load(1)); // Run the query as user 1.
    $theResults = $theNodeQuery->execute();
    if (isset($theResults['node'])) {
      return $theResults['node']; // array of node ids
    }
  }


}// End class

}// End namespace
