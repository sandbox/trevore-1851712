<?php
namespace org\drupal\ujumbe;
use \org\drupal\BaseNodeModel as BaseClass;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\RespondentModel as UjumbeRespondents;
{// Begin namespace

/**
 * Model class for Ujumbe Respondents.
 */
class CallMsgModel extends BaseClass {
  const BUNDLE_NAME = 'ujumbe_call';
  const BUNDLE_AS_URL = 'ujumbe-call';

  /**
   * @return \org\drupal\ujumbe\CallMsgModel
   */
  static public function getNew() {
    return new CallMsgModel();
  }
  
  /**
   * Gets the Drupal Bundle name for this content type.
   * Used by ancestor classes that do not have access to descendant consts.
   */
  public function getBundleName() {
    return self::BUNDLE_NAME;
  }
  
  /**
   * Gets our custom content type info for Drupal.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal custom content type info.
   */
  static public function getContentTypeInfo($t) {
    // Drupal 7 wants content type information as an associative array
    return array(
        'name' => $t('Ujumbe Call'),
        'base' => 'node_content',
        'description' => $t('Phone calls to or from Ujumbe Responders.'),
        'has_title' => '1',
        'title_label' => $t('Title'),
        'locked' => FALSE,
        'body_label' => $t('Notes'),
    );
  }

  /**
   * @return Returns the installed fields info for our content type.
   */
  public function getFieldDefinitions() {
    return array(
        // defining "body" field in case its not there for some reason
        'body' => $this->getDefaultBodyFieldDefinition(),
        'ujumbe_project_nid' => $this->getNodeIdFieldDefinition('ujumbe_project_nid', 
            UjumbeProjects::BUNDLE_NAME, TRUE),
        'ujumbe_responder_nid' => $this->getNodeIdFieldDefinition('ujumbe_responder_nid', 
            UjumbeRespondents::BUNDLE_NAME, FALSE),
    );
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::getDbSchema()
   */
  public function getDbSchema($aFieldInfo) {
    return array();
  }
  
  /**
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal field instance info for our content type.
   */
  public function getFieldInstances($t) {
    return array(
        'ujumbe_project_nid' => $this->getNodeIdFieldInstance('ujumbe_project_nid', array(
            'label' => $t('Project Node'),
            'widget' => array(
                '#readonly' => TRUE,
            ),
        )),
        'ujumbe_responder_nid' => $this->getNodeIdFieldInstance('ujumbe_responder_nid', array(
            'label' => $t('Responder Node'),
            'widget' => array(
                '#readonly' => TRUE,
            ),
        )),
        'body' => $this->getBodyFieldInstance($t, array(
            'label' => $t('Notes'),
        )),
    );

    /* TODO (see ProjectModel) using the above commented code as a template, make sure we put translatable
      * strings at the end of the method here like they do for our $t() strings.
     */
  }
  
  /**
   * Get all of the calls in the database
   *
   * @param int $aPageNum
   *   (optional) Filter the calls to this page number.
   *
   * @return
   *   A Drupal loaded entity object array of Node object results.
   */
  public function getCalls($aPageNum = -1) {
    $theNodeQuery = $this->getNewNodeQuery($aPageNum)->propertyOrderBy('nid');
    return NodeUtils::executeNodeQueryAndLoadResults($theNodeQuery);
  }
  
  /**
   * Return the total number of respondents.
   *
   * @return
   *   The number of respondents.
   */
  public function getCount() {
    return $this->getNewNodeQuery()->count()->execute();
  }
  
  /**
   * Get a new Entity object filled with required fields.
   * @param string $aPhone - where the call came from
   * @return Returns a new \EntityMetadataWrapper.
   */
  public function createNewEntity($aPhone) {
    $theNewEntity = NodeUtils::createNode(self::BUNDLE_NAME);
    $theNewEntity->title->set($this->sanitizePhoneNumber($aPhone) . '_call');
    return $theNewEntity;
  }
  
  /**
   * Insert a new Call record.
   * @param string $aPhone - where the call came from
   * @param int $aResponderId - (optional) the responder ID
   * @param int $aProjectId - (optional) the project ID
   * @return Returns the new record's ID.
   */
  public function insertCall($aPhone, $aResponderId = 0, $aProjectId = 0, $aNotes = NULL) {
    $theNewCall = $this->createNewEntity($aPhone);
    $theNewCall->ujumbe_responder_nid->set($aResponderId);
    $theNewCall->ujumbe_project_nid->set($aProjectId);
    $theNewCall->body->set($aNotes);
    $theNewCall->save();
    return $theNewCall->getIdentifier();
  }
  
}// End class

}// End namespace
