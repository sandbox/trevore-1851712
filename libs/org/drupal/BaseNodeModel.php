<?php
namespace org\drupal;
use \stdClass as BaseClass;
use \org\drupal\IDrupalContentType;
use \EntityFieldQuery;
use \org\drupal\NodeUtils;
use \EntityDrupalWrapper;
{// Begin namespace

/**
 * Abstract class for Node-based data models.
 */
abstract class BaseNodeModel extends BaseClass implements IDrupalContentType {
  const DEFAULT_MAX_PER_PAGE = 25;
  
  /**
   * @return Returns the current Drupal setting for max message entries per page.
   */
  static public function getMaxPerPage() {
    return variable_get('ujumbe_max', self::DEFAULT_MAX_PER_PAGE);
  }
  
  /**
   * Returns the bare minimum instance information for a generic field.
   * @param string $aFieldName - name of the field.
   * @param array $aModificaions - changes to the instance desired (such as label).
   * @return array Returns a Drupal field instance info array.
   */
  protected function getFieldInstance($aFieldName, $aModifications = array()) {
    return array(
        'field_name' => $aFieldName,
        'entity_type' => 'node',
        'bundle' => $this->getBundleName(),
    ) + $aModifications;
  }
  
  /**
   * We may depend on the Drupal default Body field being defined, so
   * return it as a 'body' key if required.
   * 
   * @return Returns the info needed as required by Drupal.
   */
  protected function getDefaultBodyFieldDefinition() {
    return array(
        'active' => 1,
        'cardinality' => 1,
        'deleted' => 0,
        'entity_types' => array(
            0 => 'node',
        ),
        'field_name' => 'body',
        'foreign keys' => array(
            'format' => array(
                'columns' => array(
                    'format' => 'format',
                ),
                'table' => 'filter_format',
            ),
        ),
        'indexes' => array(
            'format' => array(
                0 => 'format',
            ),
        ),
        'locked' => 0,
        'module' => 'text',
        'settings' => array(),
        'translatable' => 0,
        'type' => 'text_with_summary',
    );
  }
  
  /**
   * Gets a slightly modified standard Drupal Body field instance, put in the 'body' key.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @param array $aModificaions
   *   The changes to this Body instance desired.
   * 
   * @return Returns the standard Drupal Body field instance at 10 row height instead of 20.
   */
  protected function getBodyFieldInstance($t, $aModifications = NULL) {
    $theResult = array(
        'field_name' => 'body',
        'entity_type' => 'node',
        'bundle' => $this->getBundleName(),
        'label' => $t('Description'),
        'default_value' => NULL,
        'deleted' => 0,
        'description' => '',
        'display' => array(
            'default' => array(
                'label' => 'hidden',
                'module' => 'text',
                'settings' => array(),
                'type' => 'text_default',
                'weight' => 0,
            ),
            'teaser' => array(
                'label' => 'hidden',
                'module' => 'text',
                'settings' => array(
                    'trim_length' => 600,
                ),
                'type' => 'text_summary_or_trimmed',
                'weight' => 0,
            ),
        ),
        'required' => 0,
        'settings' => array(
            'display_summary' => 1,
            'text_processing' => 1,
            'user_register_form' => FALSE,
        ),
        'widget' => array(
            'active' => 1,
            'module' => 'text',
            'settings' => array(
                'rows' => 10,
                'summary_rows' => 5,
            ),
            'type' => 'text_textarea_with_summary',
        ),
    );
    if (!empty($aModifications) && is_array($aModifications)) {
      $theResult = array_replace_recursive($theResult, $aModifications);
    }
    return $theResult;
  }
  
  /**
   * Standard Field Definition for a NodeId EntityReference field.
   * @param string $aFieldName - name of the field.
   * @param string $aTargetBundle - linked content type name.
   * @param boolean $bUseSelectList - uses a DropDown box if TRUE (recommend <100 expected in list)
   * @return array Returns a Drupal field definition array.
   */
  protected function getNodeIdFieldDefinition($aFieldName, $aTargetBundle, $bUseSelectList = FALSE) {
    return array(
    		'field_name' => $aFieldName,
    		'type' => 'entityreference',
    		'foreign keys' => array(
    				'node' => array(
    						'columns' => array(
    								'target_id' => 'nid',
    						),
    						'table' => 'node',
    				),
    		),
    		'indexes' => array(
    				'target_id' => array(
    						0 => 'target_id',
    				),
    		),
    		'settings' => array(
    				'handler' => 'base',
    				'handler_settings' => array(
    						'behaviors' => array(
    								'views-select-list' => array(
    										'status' => ($bUseSelectList) ? 1 : 0, // if expecting > 100 respondents, keep this 0/off
    								),
    						),
    						'sort' => array(
    								'direction' => 'ASC',
    								'property' => 'title',
    								'type' => 'property',
    						),
    						'target_bundles' => array(
    								$aTargetBundle => $aTargetBundle,
    						),
    				),
    				'target_type' => 'node',
    		),
    );
  }
  
  /**
   * Returns the instance information for a NodeId type field.
   * @param string $aFieldName - name of the field.
   * @param array $aModificaions - changes to the instance desired (such as label).
   * @return array Returns a Drupal field instance info array.
   */
  protected function getNodeIdFieldInstance($aFieldName, $aModifications = NULL) {
    $theResult = array(
        'field_name' => $aFieldName,
        'entity_type' => 'node',
        'bundle' => $this->getBundleName(),
        'display' => array(
            'default' => array(
                'type' => 'entityreference_label',
                'settings' => array(
                    'link' => TRUE,
                ),
                'weight' => 5,
            ),
            'teaser' => array(
                'label' => 'above',
                'settings' => array(),
                'type' => 'hidden',
                'weight' => 0,
            ),
        ),
        'widget' => array(
            'type' => 'entityreference_autocomplete',
            'settings' => array(
                'match_operator' => 'CONTAINS',
                'path' => '',
                'size' => 60,
            ),
            //'#readonly' => TRUE, not sure this even works, if so, make a new param for this function
        ),
    );
    if (!empty($aModifications) && is_array($aModifications)) {
      $theResult = array_replace_recursive($theResult, $aModifications);
    }
    return $theResult;
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::onModuleInstall()
   */
  static public function onModuleInstall() {
    static::getNew()->installContentType();
  }
  
  /**
   * Install one of our defined content types.
   */
  public function installContentType() {
    // define fields if not already defined
    $theFieldDefinitions = $this->getFieldDefinitions();
    foreach ($theFieldDefinitions as $theFieldDefinition) {
      $theFieldInfo = field_info_field($theFieldDefinition['field_name']);
      if (empty($theFieldInfo)) {
        field_create_field($theFieldDefinition);
      }
    }
    
    // register all our fields for this particular content type
    $theFieldInstances = $this->getFieldInstances(get_t());
    foreach ($theFieldInstances as $theFieldInstance) {
      $theInstanceInfo = field_info_instance($theFieldInstance['entity_type'],
          $theFieldInstance['field_name'], $this->getBundleName());
      if (empty($theInstanceInfo)) {
        field_create_instance($theFieldInstance);
      }
    }
    watchdog('ujumbe','Installed the content type: %ctype', array('%ctype' => $this->getBundleName()));
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::onModuleUninstall()
   */
  static public function onModuleUninstall() {
    static::getNew()->uninstallContentType();
  }
  
  /**
   * Uninstall one of our defined content types.
   * @param \org\drupal\ujumbe\BaseNodeModel $aDbContentType - content type model object.
   */
  public function uninstallContentType() {
    // Get array of all affected nodes to remove
    $theSql = 'SELECT nid FROM {node} n WHERE n.type = :type LIMIT 1';
    $theResult = db_query($theSql, array(':type' => $this->getBundleName()));
    // Only remove type if there is no data, otherwise leave it all to preserve data
    if (empty($theResult)) {
      node_type_delete($this->getBundleName());
    }      
  }

  /**
   * @see \org\drupal\IDrupalContentType::defineAccessPermissions()
   */
  public function defineAccessPermissions($t, &$aPermissions) {
    // create, edit any, delete any, edit own, delete own: standard node perms
    return array();
  }

  /**
   * Helper function to return this Entity's correct permission name.
   * @return string Returns <code>$aPermissionVerb . $this->getBundleName() . ' content'</code>
   */
  public function getPermissionName($aPermissionVerb) {
    return trim($aPermissionVerb) . ' ' . $this->getBundleName() . ' content';
  }
  
  /**
   * @see \org\drupal\IDrupalContentType::buildDefaultRolePermissions()
   */
  public function buildDefaultRolePermissions($aRoleId, &$aDefaultPermissions) {
    switch ($aRoleId) {
      case DRUPAL_ADMIN_RID:
        $aDefaultPermissions[$this->getPermissionName('create')] = TRUE;
        $aDefaultPermissions[$this->getPermissionName('edit any')] = TRUE;
        $aDefaultPermissions[$this->getPermissionName('delete any')] = TRUE;
        $aDefaultPermissions[$this->getPermissionName('edit own')] = TRUE;
        $aDefaultPermissions[$this->getPermissionName('delete own')] = TRUE;
        break;
      case DRUPAL_AUTHENTICATED_RID:
        $aDefaultPermissions[$this->getPermissionName('create')] = TRUE;
        $aDefaultPermissions[$this->getPermissionName('edit own')] = TRUE;
        break;
      default:
    }
  }
  
  /**
   * Helper method the gets the minimum Drupal EntityFieldQuery object
   * for this content type.
   *
   * @param $aPageNum
   *   (optional) Restricts output to a specific page of results.
   *
   * @return \EntityFieldQuery
   *   Returns the query with pre-set conditions for this content type.
   */
  public function getNewNodeQuery($aPageNum = -1) {
    $theNodeQuery = new EntityFieldQuery();
    $theNodeQuery->entityCondition('entity_type', 'node');
    $theNodeQuery->entityCondition('bundle', $this->getBundleName());
    $theNodeQuery->propertyCondition('status', 1); // 0=not published, 1=published
    if ($aPageNum>=0) {
      $theNumPerPage = self::getMaxPerPage();
      $theNodeQuery->range($aPageNum * $theNumPerPage, $theNumPerPage);
    }
    return $theNodeQuery;
  }

  /**
   * Gets the Node given the nid.
   * @param int $aNodeId - NID to load from db.
   * @return Returns the Entity API wrapper object or NULL if not found.
   */
  public function getNode($aNodeId) {
    return entity_metadata_wrapper('node', $aNodeId);
  }
  
  /**
   * Permanently save an Entity API node.
   * In case of failures, an exception is thrown.
   * @param \EntityDrupalWrapper $aNode - The node to save.
   * @return SAVED_NEW or SAVED_UPDATED is returned depending on the 
   * operation performed.
   */
  public function saveNode($aNode) {
    return entity_save('node',$aNode);
  }
  
  /**
   * @return string Returns just the digits of the phone number.
   */
  public function sanitizePhoneNumber($aPhoneNumberEntry) {
    $thePhoneNumber = preg_replace("/\(*\)*\s*\-*\+*/", "", $aPhoneNumberEntry);
    //$thePhoneNumber = preg_replace("/^1/", "", $thePhoneNumber);
    return $thePhoneNumber;
  }
  
}// End class

}// End namespace
