<?php
namespace org\drupal;
{// Begin namespace

interface IDrupalContentType {
  
  /**
   * Factory contructor static method desired by Drupal coding standard.
   */
  static public function getNew();

  /**
   * Gets the Drupal Bundle name for this content type.
   * Used by ancestor classes that do not have access to descendant consts.
   */
  public function getBundleName();

  /**
   * Gets our custom content type info for Drupal.
   *
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *
   * @return Returns the Drupal custom content type info.
   * 
   * @see http://api.drupal.org/api/examples/field_example!field_example.module/7
   */
  static public function getContentTypeInfo($t);

  /**
   * Called from the module's hook_install()
   */
  static public function onModuleInstall();

  /**
   * Called from the module's hook_uninstall()
   */
  static public function onModuleUninstall();
  
  /**
   * @return Returns the content type fields information.
   * 
   * @see http://api.drupal.org/api/examples/field_example!field_example.module/7
   */
  public function getFieldDefinitions();
  
  /**
   * Gets the schema for specialized fields.
   * 
   * @param array $aFieldInfo
   *   The Drupal field information.
   *   
   * @return array('columns'=>array, 'indexes'=>array): 
   *   Return the column and index information required.
   *   
   * @see http://api.drupal.org/api/examples/field_example--field_example.install
   */
  public function getDbSchema($aFieldInfo);
    
  /**
   * Gets the actual field instances for our content type.
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @return Returns the Drupal field instance info for our content type.
   * 
   * @see http://api.drupal.org/api/examples/field_example!field_example.module/7
   */
  public function getFieldInstances($t);
  
  /**
   * Gets the additional permissions for this content type or array().
   * 
   * @param function $t
   *     Drupal's t() function requires special handling during install.
   *     As such, it will be passed in so we do not care about it.
   *     
   * @param array $aPermissions - the current module permissions.
   */
  public function defineAccessPermissions($t, &$aPermissions);
  
  /**
   * Build the default role permissions for this content type.
   * 
   * @param int $aRoleId
   *   The default permissions for this role ID.
   *    
   * @param array &$aDefaultPermissions
   *   The permissions array to alter and grow.
   */
  public function buildDefaultRolePermissions($aRoleId, &$aDefaultPermissions);
  
}// End interface

}// End namespace
