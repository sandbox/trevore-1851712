This is just a temporary fork to give people read only access!

This project is not yet ready for production use. There is a data storage change in the pipe that will not be compatable with this code. So feel free to play with it and what not but I DO NOT RECOMMEND THIS PROJECT FOR PRODUCTION, yet. Expect to see a documented and officially released version buy the end of 2012.

******************

This project is currently in Alpha.

Dependencies:
PHPExcell 1.7.7 or newer
http://phpexcel.codeplex.com/
This needs to be extracted to sites/all/libraries/phpexcel

EnvayaSMS 3.0 or newer
https://github.com/youngj/EnvayaSMS
This needs to be downloaded to sites/all/libraries/EnvayaSMS

LICENSE:

 Copyright 2012 IST Research

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
