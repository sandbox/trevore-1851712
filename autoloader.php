<?php
namespace org\drupal\ujumbe;
{// Begin namespace

/**
 * Class or Interface name automatically passed to this function by the PHP Interpreter; 
 * Function is capable of handling the PEAR style of naming classes, but will not use the
 * list of CLASS_AUTOLOAD_PATHS or CLASS_FILENAME_FORMATS in that case.
 * 
 * @param string $aClassName - name of class to load.
 */
function autoloader($aClassName){
  $folder_list = array(
      dirname(__FILE__) . DIRECTORY_SEPARATOR . 'libs' .  DIRECTORY_SEPARATOR,
  );

  // Only try this section if namespace not detected
  if (!strpos($aClassName,'\\')) {
    // Try the PEAR style of naming classes
    $theClassNamePath = str_ireplace('_', DIRECTORY_SEPARATOR, $aClassName);
    foreach ($folder_list as $theFolder) {
      $theClassFile = $theFolder . $theClassNamePath . '.php';
      if (is_file($theClassFile) && (include $theClassFile)) {
        return true;
      }
    }
  }

  // Class not found as PEAR style, try namespace style

  $fileNameFormat_list = array('%s.php', '%s.class.php', 'class.%s.php');
  // Convert namespace format ns\sub-ns\classname into folder paths
  $theClassNamePath = str_replace('\\', DIRECTORY_SEPARATOR, $aClassName);
  $theClassFolder = dirname($theClassNamePath) . DIRECTORY_SEPARATOR;
  $theClassName = basename($theClassNamePath);
  foreach ($folder_list as $theFolder) {
    foreach ($fileNameFormat_list as $theFileNameFormat) {
      $theFileName = sprintf($theFileNameFormat,$theClassName);
      $theClassPath = $theFolder . $theClassFolder . $theFileName;
      $theClassPathAlt = $theFolder . $theFileName;
      if (is_file($theClassPath) || is_file($theClassPathAlt)) {
        if (include $theClassPath) 
          return true;
        if (include $theClassPathAlt) 
          return true;
      }
    }
  }

  //class not found
  return false;
}

spl_autoload_register(__NAMESPACE__ .'\autoloader');
}//end namespace
