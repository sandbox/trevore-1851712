<?php
/**
 * @file
 * The install file for Ujumbe
 */

use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\RespondentModel as UjumbeRespondents;
use \org\drupal\ujumbe\TextMsgModel as UjumbeMessages;
use \org\drupal\ujumbe\CallMsgModel as UjumbeCalls;
use \org\drupal\ujumbe\RespondentGroupModel as UjumbeGroups;

// Auto-load classes as needed.
require_once('autoloader.php');

/**
 * Implements hook_install().
 */
function ujumbe_install() {
  node_types_rebuild(); // rebuild the cache
  UjumbeProjects::onModuleInstall();
  UjumbeRespondents::onModuleInstall();
  UjumbeMessages::onModuleInstall();
  UjumbeCalls::onModuleInstall();
  UjumbeGroups::onModuleInstall();
}

/**
 * Implements hook_uninstall().
 */
function ujumbe_uninstall() {
  // RTF: I can see this for debug reasons, but Drupal frowns on data removal.
  // Delete any files uploaded by the module
  //drupal_get_path('module', 'ujumbe');
  //file_unmanaged_delete_recursive();

  UjumbeProjects::onModuleUninstall();
  UjumbeRespondents::onModuleUninstall();
  UjumbeMessages::onModuleUninstall();
  UjumbeCalls::onModuleUninstall();
  UjumbeGroups::onModuleUninstall();
}

/**
 * Implements hook_enable(), fires when our module is enabled.
 *
 * Removes anonymous users access by default.
 * Gives authenticated users access by default.
 */
function ujumbe_enable() {
	$dbUjumbeProjects = UjumbeProjects::getNew();
	$dbUjumbeRespondents = UjumbeRespondents::getNew();
	$dbUjumbeMessages = UjumbeMessages::getNew();
	$dbUjumbeCalls = UjumbeCalls::getNew();
  $dbUjumbeGroups = UjumbeGroups::getNew();
	
	// anon role default rights
	$theRoleId = DRUPAL_ANONYMOUS_RID;
  $theDefaultPerms = array(
			'access content' => FALSE,
  		'access comments' => FALSE,
  		'use text format filtered_html' => FALSE,
	);
  $dbUjumbeProjects->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeRespondents->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeMessages->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeCalls->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
	$dbUjumbeGroups->buildDefaultRolePermissions($theRoleId,$theDefaultPerms);
	user_role_change_permissions($theRoleId, $theDefaultPerms);

	// auth role default rights
	$theRoleId = DRUPAL_AUTHENTICATED_RID;
	$theDefaultPerms = array(
			'access ujumbe content' => TRUE,
	);
  $dbUjumbeProjects->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeRespondents->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeMessages->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeCalls->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
	$dbUjumbeGroups->buildDefaultRolePermissions($theRoleId,$theDefaultPerms);
	user_role_change_permissions($theRoleId, $theDefaultPerms);

	// admin role default rights
	$theRoleId = DRUPAL_ADMIN_RID;
	$theDefaultPerms = array(
			'access ujumbe content' => TRUE,
			'access ujumbe admin' => TRUE,
	);
  $dbUjumbeProjects->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeRespondents->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeMessages->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
  $dbUjumbeCalls->buildDefaultRolePermissions($theRoleId, $theDefaultPerms);
	$dbUjumbeGroups->buildDefaultRolePermissions($theRoleId,$theDefaultPerms);
	user_role_change_permissions($theRoleId, $theDefaultPerms);	
}

/**
 * Implements hook_field_schema().
 * 
 * Defines the database schema of the field, using the format used by the 
 * Schema API.
 *
 * All implementations of hook_field_schema() must be in the module's .install file.
 *
 * @see http://drupal.org/node/146939
 * @see schemaapi
 * @see hook_field_schema()
 * @ingroup field_example
 * @see http://api.drupal.org/api/examples/field_example!field_example.install/7
 */
/*
function ujumbe_field_schema($aFieldInfo) {
  $theResult = array_replace_recursive(
      UjumbeProjects::getNew()->getDbSchema($aFieldInfo),
      UjumbeRespondents::getNew()->getDbSchema($aFieldInfo),
      UjumbeMessages::getNew()->getDbSchema($aFieldInfo),
      UjumbeCalls::getNew()->getDbSchema($aFieldInfo),
      UjumbeGroups::getNew()->getDbSchema($aFieldInfo),
      array()); // Pass empty array here to allow "," at end of prior line.
  return $theResult;
  // We have no special fields at this time, but when we do, see
  //   http://api.drupal.org/api/examples/field_example!field_example.module/7
}
*/
