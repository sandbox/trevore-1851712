<?php

/**
 * @file
 * The messages file for Ujumbe
 * This file implements the messages functionality
 */

use org\drupal\ujumbe\ProjectGateways as UjumbeProjects;


/**
 * Get the messages for the given project, person and page number
 * 
 * @param $page
 *   (optional) The page number to return messages for. If ommited then return all messages
 * 
 * @param $project_id
 *   (optional) The project id to return messages for. If ommited return messages for all projects
 *
 * @param $person_id
 *   (optional) The person id to return messages for. If omitted return messages for all people
 *
 * @return
 *   The mysql result of the query
 */
function _ujumbe_get_messages($page = -1, $project_id = "", $person_id = "", $inout = ""){
  $max_range = UjumbeTextMessages::getMaxPerPage();
  $query = db_select('ist_message', 'm');
  $query -> fields('m', array('message_id', 'project_id', 'person_id', 'message', 'translation', 'notes', 'source_id', 'source', 'source_table', 'timestamp', 'type'));
  $query -> orderBy('m.message_id', 'DESC');
  $query -> condition('m.archived', '0', '=');
  if($project_id) {
    $query -> condition('m.project_id', $project_id, '=');
  }
  if($person_id) {
    $query -> condition('m.person_id', $person_id, '=');
  }
  if($inout == "Incoming") {
    $query -> condition('m.type', 'Incoming', '=');
  } else if($inout == "Outgoing") {
    $query -> condition('m.type', 'Outgoing', '=');
  }
  if ($page >=0) {
    $query -> range($page * $max_range,$max_range);
  }
  $result = $query -> execute();
  return $result;
} // function _ujumbe_get_messages


/**
 * Return the details for the specified message
 * TODO This includes envaya specific details. Eventually other gateways will need to be supported
 *
 * @param $message_id
 *   The id of the message to get details for
 *
 * @return 
 *   The mysql result with the details of the specified message
 */
function _ujumbe_message_details($message_id) {
  $query = db_select('ist_message', 'm');
  $query -> fields('m', array('message_id', 'project_id', 'person_id', 'message', 'translation', 'notes', 'timestamp', 'type', 'source', 'source_id', 'archived'));
  $query -> condition('m.message_id', $message_id, '=');

  $result = $query -> execute();

  return $result;
} // function _ujumbe_message_details


/**
 * Return the total number of messages for the given person/project ID
 *
 * @param $project_id
 *   (optional) The ID of the project to return the count for
 * 
 * @param $person_id 
 *   (optional) The ID of the person to return the count for
 *
 * @return
 *   The number of messages for the given person/project
 */  
function _ujumbe_message_count($project_id = "", $person_id = "", $inout = "") {
  $query = db_select('ist_message', 'm');
  $query -> fields('m', array('message_id'));
  if($project_id) {
    $query -> condition('m.project_id', $project_id, '=');
  }
  if($person_id) {
    $query -> condition('m.person_id', $person_id, '=');
  }
  if($inout == "Incoming") {
    $query -> condition('m.type', 'Incoming', '=');
  } else if($inout == "Outgoing") {
    $query -> condition('m.type', 'Outgoing', '=');
  }
  $query -> condition('m.archived', '0', '=');
  $result = $query -> execute();
  $count = $result -> rowCount();
  return $count;
} // function _ujumbe_message_count


/**
 * Renders the messages page
 *
 * @param $project_id
 *   (optional) The id of the project to display messages for
 *
 * @param $person_id
 *   (optional) The id of the person to display messages for
 *
 * @return
 *   The html to be displayed for the given messages page
 */
function _ujumbe_messages_html_page($project_id = "", $person_id = "") {
  $page_number = 0;
  $pager_suffix = "";
  $inout = "";

  if (isset($_GET['page']) and $_GET['page'] > 0) {
    $page_number = $_GET['page'];
  }

  if (isset($_GET['projectid']) and $_GET['projectid'] > 0) {
    $project_id = $_GET['projectid'];
    $pager_suffix = "&projectid=$project_id";
  }             

  if (isset($_GET['inout']) and $_GET['inout'] != "both") {
    $inout= $_GET['inout'];
    $pager_suffix = "&inout=$inout";
  }        
  $bgcolor = "bgcolor='silver'";

  $page = "";
 
  $count = _ujumbe_message_count($project_id, $person_id, $inout);
  $result = _ujumbe_get_messages($page_number, $project_id, $person_id, $inout);

  // Dont display the send messages link when on project or people pages
  if ($project_id=="" and $person_id=="" OR isset($_GET['projectid'])){
    $page .= "\n<p>". l("Send a message", "ujumbe_send");
  }

  $page .= "<table width=100%><tr><td>";
  $page .= "<h2>Messages</h2>\n";
  $page .= "(Click on a message to get details)</td>\n";
  $page .= "<td align=right><form method=get>";
  $page .= "Filters:";
  
  $page .= "<select name=inout>";
  $page .= "<option value=both>Incoming/Outgoing</option>";
  $page .= "<option value=Incoming>Incoming</option>";
  $page .= "<option value=Outgoing>Outgoing</option>";
  $page .= "</select>";

  $page .= "<select name=projectid>";
  $page .= "<option value=0>Project</option>";
  $page .= "<option value=0>All</option>";
  // Iterate over projects to be displayed in the select box
  $theProjects = UjumbeProjects::getNew();
  $projects_result = $theProjects->getProjectGateways();
  foreach ($projects_result as $project) {
    $selected = "";
    if ($project_id == $project->nid) {
      $selected = 'selected="selected"';
    }
    $page .= "<option value=$project->nid $selected>$project->nid. $project->project_name</option>";
  }
  $page .= "<input type=submit value='Apply Filter'></form>";
  $page .= "</td></tr></table>";
  $page .= "<table><tr><th>Message ID(Type)</th><th>Project</th><th>Person</th><th>Message<br>(Translation)<br>[Notes]</th><th>Source</th><th>Timestamp</th></tr>\n";

  foreach ($result as $message) {
    $project_name = _ujumbe_project_name($message->nid);
    $person_namenum = _ujumbe_person_namenum($message->person_id);

    $page .= "<tr $bgcolor>\n";
    $page .= "<td>".l("$message->message_id ($message->type)", "ujumbe_message_details/$message->message_id")."</td>\n";
    $page .= "<td>".$project_name."</td>\n";
    $page .= "<td>".$person_namenum."</td>\n";

    $message_text = $message->message. "\n";
    $message_text2 = "";
    if (strlen($message->translation) > 0) {
      $message_text2 = " (" . $message->translation . ") ";
    }
    $message_text3 = "";
    if (strlen($message->notes) > 0) {
      $message_text3 = " [" . $message->notes . "] ";
    }

    $page .= "<td>".l($message_text, "ujumbe_message_details/$message->message_id")."\n";
    $page .= "<br>".l($message_text2, "ujumbe_message_details/$message->message_id")."\n";
    $page .= "<br>".l($message_text3, "ujumbe_message_details/$message->message_id")."</td>\n";
    $page .= "<td>".l($message->source, "ujumbe_message_details/$message->message_id")."</td>\n";
    $page .= "<td>".l($message->timestamp, "ujumbe_message_details/$message->message_id")."</td>\n";
    $page .= "</tr>\n";

    if ($bgcolor == "bgcolor='silver'") {
      $bgcolor = "";
    } else {
      $bgcolor = "bgcolor='silver'";
    }
  }

  $page .= "</table>";
  $page .= "Total: $count\n";
  $page .= "(";
  if ($project_id) {
    $page .= l('Export Messages', "ujumbe_export_messages/$project_id");
  } elseif ($person_id) {
    $page .= l('Export Messages', "ujumbe_export_messages/0/$person_id");
  } else {
    $page .= l('Export Messages', 'ujumbe_export_messages');
  }
  $page .= ")";
  $page .= _ujumbe_pager($page_number,$count, "get" , "", $pager_suffix);

  return $page;
} // function _ujumbe_messages_html_page


/**
 * This function exports all of the messages in the system into a excel .xls file
 * 
 * The reason I went with xls instead of a csv is because csv files technically do 
 * not support anything other then ASCII. So no UTF support.
 * 
 * The file is downloaded to the requesting computer and then the code exits. 
 * So no new page is displayed.
 * The person and project ID are passed are arguments in the URL
 */
function _ujumbe_export_messages_xls(){
  require_once dirname(__DIR__)."/../../libraries/phpexcel/Classes/PHPExcel.php";
  $project_id = arg(1);
  $person_id= arg(2);
  if ($project_id>0) {
    $messages= _ujumbe_get_messages(-1, $project_id);
  } elseif($person_id > 0) {
    $messages= _ujumbe_get_messages(-1, "", $person_id);
  } else {
    $messages= _ujumbe_get_messages();
  }

  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getActiveSheet()->setTitle('Ujumbe Messages');
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Message ID')
            ->setCellValue('B1', 'Project ID')
            ->setCellValue('C1', 'Person ID')
            ->setCellValue('D1', 'Message')
            ->setCellValue('E1', 'Translation')
            ->setCellValue('F1', 'Notes')
            ->setCellValue('G1', 'Source ID')
            ->setCellValue('H1', 'Source')
            ->setCellValue('I1', 'Source Table')
            ->setCellValue('J1', 'Timestamp');
            
  // Loop through the result set
  $rowNumber = 2;
  while (($row = $messages->fetchAssoc()) !== FALSE) {
     $col = 'A';
     foreach($row as $cell) {
        $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$cell);
        $col++;
     }
     $rowNumber++;
  }

  // Freeze pane so that the heading line won't scroll
  $objPHPExcel->getActiveSheet()->freezePane('A2');

  // Save as an Excel BIFF (xls) file
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment;filename="Ujumbe_Messages' . date('Ymd') . '.xls"');
 header('Cache-Control: max-age=0');

 $objWriter->save('php://output');
 exit();
} // function _ujumbe_export_messages_xls

/**
 * Implements hook_form()
 * 
 * A form to send messages
 * @param $project_id
 *   (Optional) The ID of the project
 * @param $number
 *   (Optional) The phone number of the recipent.
 * @param $message
 *   (Optional) The message to send

 */
function _ujumbe_send_form($form, &$form_statte, $project_id = "", $number = "", $message = "", $group = "") {
  if ($group == "group") { 
    $groups_result = _ujumbe_get_groups();
    $group_options = array();                                                           
    foreach ($groups_result as $group_res) {
      $group_options[$group_res->group_id] = t($group_res->group_id . ". " . $group_res->group_name);
    }
  }

  $dbProjects = UjumbeProjects::getNew();
  $projects_result = $dbProjects->getActiveProjectGateways();
  $max_length = variable_get('ujumbe_max_length',160);
  $options = array();
  foreach ($projects_result as $project) {
    $options[$project->project_id] = t($project->project_id . ". " . $project->project_name);
  }
  
  $form['send'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',                                                                                                  
  );
  $form['project_id'] = array(
    '#type' => 'select',
    '#title' => "Project",
    '#options' => $options,
    '#default_value' => $project_id,
    '#size' => 3,
  );
  if ($group == "group") { 
    $form['group_id'] = array(
      '#type' => 'select',
      '#title' => "Group",
      '#options' => $group_options,
      '#default_value' => $number,
    );
  $form['group_send'] = array(
    '#type' => 'hidden',                                                                                             
    '#title' => "Group Send Flag",
    '#value' => TRUE,
  );

  } else {
    $form['number'] = array(
      '#type' => 'textfield',
      '#title' => "Phone Number(s). Comma separated if more then one.",
      '#default_value' => $number,
      '#size' => 20,
    );
  }
 $form['message'] = array(
    '#type' => 'textfield',
    '#title' => "Message",
    '#default_value' => $message,
    '#maxlength' => $max_length,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Send Message",
  );

  return $form;
} // function _ujumbe_send_form

/**
 * Implements hook_form_subit()
 * 
 * Updates a person's info
 */
function _ujumbe_send_form_submit($form, &$form_state) {
  $project_id= $form_state['values']['project_id'];
  $message= $form_state['values']['message'];

  if (isset($form_state['values']['group_send']) && $form_state['values']['group_send']) {
    $group_id = $form_state['values']['group_id'];
    _ujumbe_group_send($project_id, $group_id, $message);
  } else { 
    $number = $form_state['values']['number'];
    _ujumbe_send($project_id, $number, $message);
  }
} // function _ujumbe_send_form_submit

/**
 * Implements hook_form_validate().
 */
function _ujumbe_send_form_validate($form, &$form_state){
  $project_id = $form_state['values']['project_id'];
  $message= $form_state['values']['message'];
  $max_send = variable_get('ujumbe_max_send',25);
  $max_length = variable_get('ujumbe_max_length',160);

  if ($project_id == ""){
    form_set_error('project_id', t('You must select a project.'));
  }
  
  if (isset($form_state['values']['group_send']) && $form_state['values']['group_send']) {
    $group_id= $form_state['values']['group_id'];
    if (! $group_id > 0) {
      form_set_error('group_id', t('A valid group must be selected.'));
    }
  } else {
    $number = $form_state['values']['number'];
    if ($number == ""){
      form_set_error('number', t('You must enter at least one number to send the message to.'));
    }
    if (!is_numeric($number)) {
      $numbers_array = preg_split("/\,/", $number);
      $count = count($numbers_array);
      if ($count > $max_send) {
          form_set_error('number', t("You may not send messages to more then $max_send recipents at a time, $count people being sent to presently."));
      }
      foreach ($numbers_array as $snumber) {
        if (!is_numeric($snumber)) {
          form_set_error('number', t('Please only use numbers to enter the phone number. No spaces, -, () or +'));
        }
      }
    }
  }
  if (($max_length) && (strlen($message) > $max_length)) {
    form_set_error('message', t("The message cannot be longer then $max_length caracters."));
  }
  if ($message == ""){
    form_set_error('message', t('You can\'t send a blank message.'));
  }
} // function ujumbe_settings_form_validate      

/**
 * Sends a message
 * 
 * @param $project_id
 *   The ID of the project
 * @param $number
 *   The phone number of the recipent.
 * @param $message
 *   The message to send
 *
*/
function _ujumbe_send($project_id, $numbers, $message) {
  $numbers_array = preg_split("/\,/", $numbers);
  foreach ($numbers_array as $number) {
    $normal_number = _ujumbe_normal_number($number);

    $query = 'INSERT INTO {ist_envaya_outgoing} SET project_id= :project_id, number = :number, message = :message';
    $values= array(':project_id'=>$project_id, ':number'=>$normal_number, ':message'=> $message);
    db_query($query,$values);

    $query2 = "SELECT LAST_INSERT_ID() AS id";
    $result = db_query($query2);
    $row = $result->fetchAssoc();
    $message_id = $row['id'];
    _ujumbe_insert_person($normal_number);
    _ujumbe_insert_message(
          $project_id, 
          $normal_number,
          $message, 
          $message_id, 
          "Envaya", 
          "ist_envaya_outgoing",
          "Outgoing"                                                                                                
        );
  }

  drupal_set_message(t("Message sent."));
} //function _ujumbe_send

/**
 * Renders a page containing a form to send messages from
 * 
 * @param arg(1)$project_id
 *   (Optional) The ID of the project
 * @param arg(2)$number
 *   (Optional) The phone number of the recipent. If this is a group, it should be the number of the group being sent a message
 * @param arg(3)$message
 *   (Optional) The message to send
 * @param arg(4)$group
 *   (Optional) Set to "group" if this is a group message
 *
 * @return
 *   The html containing the message details to be displayed
 */
function _ujumbe_send_html_page() {
  $project_id= arg(1);
  $number= arg(2);
  $message = arg(3);
  $group= arg(4);

  $page = "Send Page";
  $get_form = drupal_get_form('_ujumbe_send_form', $project_id, $number, $message, $group);
  $page .= drupal_render($get_form);
  return $page;
} // function _ujumbe_send_html_page


/**
 * Renders a page containing all the details of a specified message
 * 
 * @param arg(1)$message_id
 *   The ID of the message to be displayed. This is passed as an argument in the URL
 *
 * @return
 *   The html containing the message details to be displayed
 */
function _ujumbe_message_details_html_page() {
  $message_id = arg(1);
  if(isset($message_id) and $message_id > 0) {

    $result = _ujumbe_message_details($message_id);
    $message =  $result->fetchAssoc();
    $project_id = $message['project_id'];
    $project_name = _ujumbe_project_name($project_id);
    $person_id = $message['person_id'];
    $person_namenum= _ujumbe_person_namenum($person_id);
    $person_num = _ujumbe_get_person_phone($person_id);
  
    $page = "";
    $get_form = drupal_get_form('_ujumbe_message_details_form');
    $page .= drupal_render($get_form);


    if ($message['source'] == "Envaya" and $message['type'] == "Incoming") {
      $eresult = _ujumbe_envaya_incoming_message_details($message['source_id']);
      $emessage =  $eresult->fetchAssoc();
      $page .= "<hr><center><h3>Envaya specific details</h3></center>";
      $page .= "Envaya Message ID: " . $emessage['envaya_message_id'] . "<br>\n";
      $page .= "Action: " . $emessage['action'] . "<br>\n";
      $page .= "Version: " . $emessage['version'] . "<br>\n";
      $page .= "Phone N: " . $emessage['phone_number'] . "<br>\n";
      $page .= "Log: " . $emessage['log'] . "<br>\n";
      $page .= "Send Limit: " . $emessage['send_limit'] . "<br>\n";
      $page .= "Settings Version: " . $emessage['settings_version'] . "<br>\n";
      $page .= "Battery: " . $emessage['battery'] . "<br>\n";
      $page .= "Power: " . $emessage['power'] . "<br>\n";
      $page .= "Network: " . $emessage['network'] . "<br>\n";
      $page .= "Message Type: " . $emessage['message_type'] . "<br>\n";
      $page .= "From Phone: " . $emessage['from_phone'] . "<br>\n";
    } else if ($message['source'] == "Envaya" and $message['type'] == "Outgoing") {
      $eresult = _ujumbe_envaya_outgoing_message_details($message['source_id']);
      $emessage =  $eresult->fetchAssoc();
      $page .= "<hr><center><h3>Envaya specific details</h3></center>";
      $page .= "Envaya outgoing ID: " . $emessage['envaya_outgoing_id'] . "<br>\n";
      $page .= "Status: " . $emessage['status'] . "<br>\n";
    }
    $page .= "<hr><center><h3>Reply</h3></center>\n";
    $get_form = drupal_get_form('_ujumbe_send_form', $project_id, $person_num);
    $page .= drupal_render($get_form);
  } 

  return $page;
} // function _ujumbe_message_details_html_page

/**
 * Implements hook_form()
 *
 * A form to edit the details of a message
 */
function _ujumbe_message_details_form($form, &$form_state) {
  $message_id = arg(1);
  $result = _ujumbe_message_details($message_id);
  $message =  $result->fetchAssoc();
 
  $project_id = $message['project_id'];
  $project_name = _ujumbe_project_name($project_id);
  $person_id = $message['person_id'];
  $person_namenum= _ujumbe_person_namenum($person_id);

  $form['message'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['message_id'] = array(
    '#type' => 'hidden',
    '#title' => "Message ID",
    '#value' => $message['message_id'],
  );

  $form['message_id_item'] = array(
    '#type' => 'item',
    '#title' => "Message ID",
    '#markup' => $message['message_id'],
    '#value' => $message['message_id'],
  );

  $form['message_type_item'] = array(
    '#type' => 'item',
    '#title' => "Type",
    '#markup' => $message['type'],
    '#value' => $message['type'],
  );

  $form['message_project_id_item'] = array(
    '#type' => 'item',
    '#title' => "Project ID",
    '#markup' => "$project_id ($project_name)",
    '#value' => $project_id,
  );

  $form['message_person_id_item'] = array(
    '#type' => 'item',
    '#title' => "Person ID",
    '#markup' => "$person_id ($person_namenum)",
    '#value' => $person_id,
  );

  $form['message_message_item'] = array(
    '#type' => 'item',
    '#title' => "Message",
    '#markup' => $message['message'],
    '#value' => $message['message'],
  );

  $form['message_translation'] = array(
    '#type' => 'textfield',
    '#title' => "Translation",
    '#default_value' => $message['translation'],
    '#size' => 100,
  );

  $form['message_notes'] = array(
    '#type' => 'textfield',
    '#title' => "Notes",
    '#default_value' => $message['notes'],
    '#size' => 100,
  );

  $form['message_timestamp_item'] = array(
    '#type' => 'item',
    '#title' => "Timestamp",
    '#markup' => $message['timestamp'],
    '#value' => $message['timestamp'],
  );

  $form['message_source_item'] = array(
    '#type' => 'item',
    '#title' => "Source",
    '#markup' => $message['source'],
    '#value' => $message['source'],
  );
  
  $form['message_archived'] = array(
    '#type' => 'select',
    '#title' => "Archived",
    '#options' => array(
      '0' => t('False'),
      '1' => t('True'),
    ),
    '#default_value' => $message['archived'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Submit Changes",
  );

  return $form;
} // function _ujumbe_message_details_form

/**                                                                                                     
 * Implements hook_form_submit()
 *
 * Updates the details of a message
 */
function _ujumbe_message_details_form_submit($form, &$form_state) {
  $message_id = $form_state['values']['message_id'];
  $translation = $form_state['values']['message_translation'];
  $notes = $form_state['values']['message_notes'];
  $archived = $form_state['values']['message_archived'];

  $query = 'UPDATE {ist_message} SET translation = :translation, notes = :notes, archived = :archived  WHERE message_id = :message_id';
  $values= array(':translation'=>$translation, ':notes'=> $notes, ':archived'=> $archived, ':message_id'=>$message_id);
  db_query($query,$values);

  drupal_set_message(t("Message updated."));
//  drupal_set_message("<pre>" .print_r($form_state, TRUE) . "</pre>");
} // function _ujumbe_project_details_form_submit


/**
 * Inserts a message into the messages table
 *
 * @param $project_id
 *   The ID of the project
 *
 * @param $from_phone
 *   The sender's phone number
 *
 * @param $message
 *   The body of the text message
 *
 * @param $source_id
 *   The ID of the entry in the source gateway table
 *
 * @param $source
 *   The name of the source gateways type
 *
 * @param $source_table
 *   The table name for the source gateway
 */
function _ujumbe_insert_message($project_id, $from_phone, $message, $source_id, $source, $source_table, $type = "Incoming", $archived = 0) {
  $person_id = _ujumbe_insert_person($from_phone);
  $query='INSERT INTO {ist_message} (project_id, person_id, message, source_id, source, source_table, type, archived) VALUES (:project_id, :person_id, :message, :source_id, :source, :source_table, :type, :archived)';
  $values= array(':project_id'=>$project_id, ':person_id'=>$person_id, ':message'=>$message, ':source_id'=>$source_id, ':source'=>$source, ':source_table'=>$source_table, ':type'=>$type, ':archived'=>$archived);
  db_query($query, $values);
} // function _ujumbe_insert_message

/**
 * Handles incoming messages
 *
 * @param $project_id
 *   The ID of the project
 *
 * @param $number
 *   The sender's phone number
 *
 * @param $message
 *   The body of the text message
 *
 * @param $source_id
 *   The ID of the entry in the source gateway table
 *
 * @param $source
 *   The name of the source gateways type
 *
 * @param $source_table
 *   The table name for the source gateway
 */
function _ujumbe_incoming_message($project_id, $number, $message, $source_id, $source, $source_table, $archived = 0) {
  $normal_number = _ujumbe_normal_number($number);
  _ujumbe_insert_message($project_id, $normal_number, $message, $source_id, $source, $source_table, "Incoming", $archived);
  _ujumbe_ushahidi_process($project_id, $message, $normal_number);
} // function _ujumbe_incoming_message
