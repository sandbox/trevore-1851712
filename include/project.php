<?php
/**
 * @file
 * The project file for Ujumbe
 * This file implements the project functionality
 */

use \org\drupal\NodeUtils;
use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;

/** 
 * Gets the timestamp for the most recent message for a given project
 *
 * @param $project_id
 *   The ID of the project
 *
 * @return
 *   The timestamp
 */
function _ujumbe_project_recent_timestamp($project_id) {
  $query= "SELECT timestamp FROM {ist_message} WHERE project_id = $project_id ORDER BY timestamp DESC LIMIT 1";
  $result = db_query($query);
  $recent = $result -> fetchField();
  return $recent;
} // function _ujumbe_project_recent_timestamp

/**
 * The details for a given projects
 *
 * @param $project_id
 *   The ID of the project
 *
 * @return 
 *   A mysql result with the details of the project
 */
function _ujumbe_project_details($project_id) {
  $query = db_select('ist_project', 'p');
  $query -> fields('p', array('project_id', 'project_name', 'project_description', 'project_status', 'gateway_power', 'gateway_network', 'gateway_time', 'timestamp', 'ushahidi_enabled', 'ushahidi_url', 'auto_reply'));
  $query -> condition('p.project_id', $project_id, '=');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_project_details


/**
 * Gets the name of a project
 *
 * @param $aProjectId
 *   The ID of the project
 *
 * @returns
 *   The name of the project
 */
function _ujumbe_project_name($aProjectId) { 
  $dbProjects = UjumbeProjects::getNew();
  $theProject = $dbProjects->getNode($aProjectId);
  return l($theProject->title, "node/$aProjectId");
} // function _ujumbe_project_name

/**
 * Updates the status of a gateway on a project
 *
 * @param $aProjectId
 *   The ID of the project
 *
 * @param $aGatewayPower
 *   Text describing what kind of power source.
 *   
 * @param $aGatewayNetwork
 *   Text describing the kind of network connection.
 *   
 * @param $passive
 *   ???
 */
function _ujumbe_project_update_gateway_status($aProjectId, $aGatewayPower = "Unknown", $aGatewayNetwork = "Unknown", $passive = 0) {
  $dbProjects = UjumbeProjects::getNew();
  $theProject = $dbProjects->getNode($aProjectId);
  $theProject->ujumbe_gateway_power = $aGatewayPower;
  $theProject->ujumbe_gateway_network = $aGatewayNetwork;
  $theProject->ujumbe_project_status = 'Active';
  node_save($theProject);
}

/**
 * Implements hook_form_submit()
 *
 * Updates the details of a project
 */
function _ujumbe_project_details_form_submit($form, &$form_state) {
  $project_id = $form_state['values']['project_id'];
  $project_name = $form_state['values']['project_name'];
  $project_description = $form_state['values']['project_description'];
  $project_status = $form_state['values']['project_status'];
  $ushahidi_enabled = $form_state['values']['ushahidi_enabled'];
  $ushahidi_url = $form_state['values']['ushahidi_url'];
  $auto_reply= $form_state['values']['auto_reply'];

  $query = 'UPDATE {ist_project} SET project_name = :project_name, project_description = :project_description, project_status = :project_status, ushahidi_enabled = :ushahidi_enabled, ushahidi_url = :ushahidi_url, auto_reply = :auto_reply WHERE project_id = :project_id';
  $values= array(':project_name'=>$project_name, ':project_description'=> $project_description, ':project_status'=>$project_status, ':project_id'=>$project_id, ':ushahidi_enabled'=>$ushahidi_enabled, ':ushahidi_url'=>$ushahidi_url, ':auto_reply'=>$auto_reply);
  db_query($query,$values);

  drupal_set_message(t("Project updated."));
//  drupal_set_message("<pre>" .print_r($form_state, TRUE) . "</pre>");
} // function _ujumbe_project_details_form_submit


/**
 * Implements hook_form()
 *
 * A form to edit the details of a project
 */
function _ujumbe_project_details_form($form, &$form_state) {
  $project_id=arg(1);
  global $base_url;


  $result = _ujumbe_project_details($project_id);
  $project =  $result->fetchAssoc();
  $recent = _ujumbe_project_recent_timestamp($project_id);
  $envaya_url = "$base_url/envaya/?id=$project_id";

  $project['lastmessage'] = $recent;
 
  $form['project'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['project_id'] = array(
    '#type' => 'hidden',
    '#title' => "Project ID",
    '#value' => $project['project_id'],
  );

  $form['project_id_item'] = array(
    '#type' => 'item',
    '#title' => "Project ID",
    '#markup' => $project['project_id'],
    '#value' => $project['project_id'],
  );

  $form['project_envaya_url'] = array(
    '#type' => 'item',
    '#title' => "Envaya URL",
    '#markup' => $envaya_url,
    '#value' => $envaya_url,
  );

  if (user_access('access ujumbe admin')) {
    $form['project_name'] = array(
      '#type' => 'textfield',
      '#title' => "Project Name",
      '#default_value' => $project['project_name'],
    );
  } else {
    $form['project_name'] = array(
      '#type' => 'item',
      '#title' => "Project Name",
      '#markup' => $project['project_name'],
    );
  }

  if (user_access('access ujumbe admin')) {
    $form['project_status'] = array(
      '#type' => 'select',
      '#title' => "Project Status",
      '#options' => array(
        'Active' => t('Active'),
        'Inactive' => t('Inactive'),
      ),
      '#default_value' => $project['project_status'],
    );
  } else {
    $form['project_status'] = array(
      '#type' => 'item',
      '#title' => "Project Status",
      '#markup' => $project['project_status'],
    );
  }

  if (user_access('access ujumbe admin')) {
    $form['project_description'] = array(
      '#type' => 'textarea',
      '#title' => "Project description",
      '#default_value' => $project['project_description'],
      '#description' => "The description should include the exact text of any question asked as well as any other intformation that may be relevent to analysis down the road",
    );
  } else {
    $form['project_description'] = array(
      '#type' => 'item',
      '#title' => "Project description",
      '#markup' => $project['project_description'],
    );
  }

  if (user_access('access ujumbe admin')) {                                                  
    $form['auto_reply'] = array(
      '#type' => 'textfield',
      '#title' => "Automatic Reply Text",
      '#default_value' => $project['auto_reply'],
      '#description' => "The text of the automatic reply to messages, leave blank for no reply. The first instance of %m will be replaced with the text of the incoming message. The text of the incoming message will be truncated to make sure the reply does not go over the max length.",
    );
  } else {
    $form['auto_reply'] = array(
      '#type' => 'item',
      '#title' => "Automatic Reply Text",
      '#description' => "The text of the automatic reply to messages, leave blank for no reply",
      '#markup' => $project['auto_reply'],
    );
  }

  if (user_access('access ujumbe admin')) {                                                  
    $form['ushahidi_url'] = array(
      '#type' => 'textfield',
      '#title' => "Ushahidi URL",
      '#default_value' => $project['ushahidi_url'],
      '#description' => "The FrontlineSMS URL for an Ushahidi project to push messages to.",
    );
  } else {
    $form['ushahidi_url'] = array(
      '#type' => 'item',
      '#title' => "Ushahidi URL",
      '#description' => "The FrontlineSMS URL for an Ushahidi project to push messages to.",
      '#markup' => $project['ushahidi_url'],
    );
  }

 if (user_access('access ujumbe admin')) {
    $form['ushahidi_enabled'] = array(
      '#type' => 'select',
      '#title' => "Ushahidi Status",
      '#options' => array(
        '0' => t('Disabled'),
        '1' => t('Enabled'),
      ),
      '#default_value' => $project['ushahidi_enabled'],
    );
  } else {
    $form['ushahidi_enabled'] = array(
      '#type' => 'item',
      '#title' => "Ushahidi Status",
      '#markup' => $project['project_status'],
    );
  }

  $form['gateway_power'] = array(
    '#type' => 'item',
    '#title' => "Gateway Power",
    '#markup' => $project['gateway_power'],
  );

  $form['gateway_network'] = array(
    '#type' => 'item',
    '#title' => "Gateway Status",
    '#markup' => $project['gateway_network'],
  );

  $form['gateway_time'] = array(
    '#type' => 'item',
    '#title' => "Gateway Last Seen",
    '#markup' => $project['gateway_time'],
  );

  $form['timestamp'] = array(
    '#type' => 'item',
    '#title' => "Created",
    '#markup' => $project['timestamp'],
  );
  $form['lastmessage'] = array(
    '#type' => 'item',
    '#title' => "Most Recent Message",
    '#markup' => $project['lastmessage'],
  );

  if (user_access('access ujumbe admin')) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => "Submit Changes",
    );
  }
  return $form;
} // function _ujumbe_project_details_form


/**
 * Renders a html page for the details of a project
 *
 * @return
 *   Returns the html for the details of a project
 */
function _ujumbe_project_details_html_page() {
  $project_id=arg(1);
  if(isset($project_id) and $project_id > 0) {

    $page = "";
    $get_form = drupal_get_form('_ujumbe_project_details_form');
    $page .= drupal_render($get_form);
    $page .=  _ujumbe_messages_html_page($project_id);

  } 

  return $page;
} // function _ujumbe_project_details_html_page


/** 
 * Renders a html page with a list of all the projects
 *
 * @param $_GET['page']
 *   (optional) The page number
 *
 * @return
 *   Returns the html for listing all the projects
 */
function _ujumbe_projects_html_page() {
  $page_number = 0;
  if (isset($_GET['page']) and $_GET['page'] > 0) {
    $page_number = $_GET['page'];
  }
  
  $dbProjects = UjumbeProjects::getNew();
  $count = $dbProjects->getCount();
  $result = $dbProjects->getProjects($page_number);

  $theLang = 'und';
  $delta = 0;
  $bgcolor = 'bgcolor="silver"';
  $page = "";
  $page .= "<h2>Projects</h2>(Click on a project to get details)\n";
  $page .= "<table><tr><th>Project ID <br>(Status)</th><th>Project Name <br>(Gateway Status)</th><th>Project Description</th><th>Created<br>(Gateway Last Seen)</th></tr>\n";
  foreach ((array)$result as $project) {
    $page .= "<tr $bgcolor>\n";
    $page .= "<td><a href='node/$project->nid'>" . $project->nid . "<br>(" . NodeUtils::renderNodeField($project, 'ujumbe_project_status') . ")</a></td>\n";
    $page .= "<td><a href='ujumbe_project_details/$project->nid'>" . $project->title . "<br>(". NodeUtils::renderNodeField($project, 'ujumbe_gateway_power') . " | " . NodeUtils::renderNodeField($project, 'ujumbe_gateway_network') . ")</a></td>\n";
    $page .= "<td><pre><a href='node/$project->nid'>" . NodeUtils::renderNodeField($project, 'body', 'teaser') . "</a></pre></td>\n";
    $page .= "<td width=150><a href='node/$project->nid'>" . date('r', $project->created) . "<br>(" . date('r', NodeUtils::getNodeField($project,'ujumbe_gateway_time')) . ")</a></td>\n";
    $page .= "</tr>\n";

    $bgcolor = (empty($bgcolor)) ? 'bgcolor="silver"' : '';
  }

  $page .= "<tr><td>Total: $count\n</td></tr>";
  $page .= "</table>\n";
  $page .= _ujumbe_pager($page_number,$count);
  if(user_access('access ujumbe admin')) {
    $page .= '<a href="node/add/' . UjumbeProjects::BUNDLE_AS_URL . '">New Project</a>';
  }
  return $page;
} // function _ujumbe_projects_html_page
