<?php

/**
 * @file
 * The util file for Ujumbe
 * This file implements various utility functions not kept else where
 */

use \org\drupal\ujumbe\ProjectModel as UjumbeProjects;
use \org\drupal\ujumbe\TextMsgModel as UjumbeMessages;

/**
 * Creates the links at the bottom of the page for next, first, last, etc.
 *
 * @param $current_page
 *   The curent page
 *
 * @param $total_entities
 *   The total number of items that will be paged through
 *
 * @param $format
 *   (optional) Is the page number being passed as a get paramater or an arg
 *
 * @param $prefix
 *   (optional) The prefix of the URL used to build the links for the pager  
 *
 * @return
 *   The html for the pager
 */
function _ujumbe_pager($current_page, $total_entities, $format = "get", $prefix = "", $suffix = "") {
  $display_num = 10; // The number of pages to display numerically
  $next_page = $current_page +1;
  $prev_page = $current_page -1;
  $max_per_page = UjumbeMessages::getMaxPerPage();
  if ($max_per_page > 0) {
    $total_pages = ceil($total_entities / $max_per_page) -1;
  } else { 
    return;
  }
  if($total_pages == 0) { 
    return;
  }

  $pager = "<center> ";
  if ($current_page > 0) {
    if($format == "arg") { 
      $pager .= l("<< first ", "$prefix/0$suffix"); 
    } else {
      $pager .= "<a href='?page=0$suffix'>";
      $pager .= "<< first </a>";
    }

    $pager .= "&nbsp;&nbsp;&nbsp;";

    if($format == "arg") { 
      $pager .= l("< previous", "$prefix/$prev_page$suffix"); 
    } else {
      $pager .= "<a href='?page=$prev_page$suffix'>";
      $pager .= "< previous </a>";
    }
  }
  $pager .= "&nbsp;&nbsp;&nbsp;";
  if ($current_page > 0) {
    $pager .= "... ";
  }
  if ($current_page < $display_num/2) {
    $i_start = 0;
  } else {
    $i_start = $current_page;
  }
  $final_i="";
  for($i=$i_start; $i<$total_pages+1 and $i-$current_page < $display_num; $i++) { 
    if ($i != $current_page) {
      if($format == "arg") { 
        $pager .= l( "$i ", "$prefix/$i$suffix"); 
      } else {
        $pager .= " <a href='?page=$i$suffix'>$i</a> ";
      }
    } else {
      $pager .= " $i ";
    }
    $final_i = $i;
  }
  if ($final_i < $total_pages) {
    $pager .= " ...";
  }

  if ($current_page < $total_pages) {
    $pager .= "&nbsp;&nbsp;&nbsp;";

    if($format == "arg") { 
      $pager .= l("next >", "$prefix/$next_page$suffix"); 
    } else {
      $pager .= "<a href='?page=$next_page$suffix'>";
      $pager .= "next > </a>";
    }

    $pager .= "&nbsp;&nbsp;&nbsp;";

    if($format == "arg") { 
      $pager .= l("last >>", "$prefix/$total_pages$suffix"); 
    } else {
      $pager .= "<a href='?page=$total_pages$suffix'>";
      $pager .= "last >> </a>";
    }
  }
  $pager .= "</center>";

  return $pager;
} // function _ujumbe_pager

/**
 * Gets the number of entries in a given table
 *
 * @param $table
 *   The name of the table to count
 *
 * @return
 *   The number of entries in the given table
 */
function _ujumbe_table_count($table) {
  $query = "SELECT count(*) FROM {{$table}}";
  $result = db_query($query);
  $count = $result->fetchField();
  return $count;
} // function _ujumbe_table_count

/**
 * Sanatize and normalize phone numbers
 *
 * @param $number
 *  The number to be normalized
 *
 * @return 
 *  The sanatized number
 */
function _ujumbe_normal_number($number) {
  $sane = preg_replace("/\(*\)*\s*\-*\+*/", "", $number);
//  $normal = preg_replace("/^1/", "", $sane);
  return $sane;
} // function _ujumbe_normal_number

/**
 * Write a message to the ujumbe log
 *
 * @param $module
 *   Name of the module
 * 
 * @param $message
 *   Message to write to the log
 */
function _ujumbe_write_log($module, $message) {
  $query = 'INSERT INTO {ist_log} (module, message) VALUES (:module, :message)';
  $values = array(':module'=>$module, ':message'=>$message);
  db_query($query, $values) or error_log('Query failed: ' . mysql_error() . '\n<br>Query:' . $query);
} // function _ujumbe_write_log

/**
 * Calculates the elapsed time since the given timestamp
 *
 * @param $timestamp
 *   The time stampt to calculate from in unix time
 *
 * @return
 *   A string containing the elapsed time
 */
function _ujumbe_elapsed_time($timestamp) {
  $time = time() - $timestamp;
  if ($time > 315576000) {
    return "Never";
  }
  $a = array('Decade' => 315576000, 'Year' => 31557600, 'Month' => 2629800, 'Week' => 604800, 'Day' => 86400, 'Hour' => 3600, 'Min' => 60, 'Sec' => 1);
  $i = 0;
    foreach($a as $k => $v) {
      $$k = floor($time/$v);
      if ($$k) $i++;
      $time = $i >= 2 ? 0 : $time - $$k * $v;
      $s = $$k > 1 ? 's' : '';
      $$k = $$k ? $$k.' '.$k.$s.' ' : '';
      @$result .= $$k;
    }
  return $result ? $result.'ago' : 'Now';
} // function _ujumbe_elapsed_time

/**
 * Check to see if the phone has been disconnected for a extended period
 * Send an email if it has.
 *
 * @param $project_id
 *   The name of the table to count
 */
function _ujumbe_phone_check() {
  global $base_url;
  $site_name = variable_get('site_name', '');
  $theProjects = UjumbeProjects::getNew();
  $now = getdate(); 
  $send = TRUE;
  $subject = "Ujumbe warning: $site_name gateway not seen for long time.";
  $gateway_delay_notice = variable_get('ujumbe_gateway_notice_delay', '60') * 60;

  // IF the Delay notice is set to 0, then do not send notices.
  if ($gateway_delay_notice == 0 ) {
    return;
  }

  foreach ($theProjects->getProjects() as $project) {
    //$gateway_time = strtotime($project->gateway_time);
    $gateway_delay = time() - $gateway_time;
    if ($gateway_delay > $gateway_delay_notice AND $gateway_delay < 1351727962 AND $project->project_status == "Active") {
      $message = "Warning: The Phone Gateway for project " . $project->nid . " has not been seen for over " . round(($gateway_delay/60)/60) . " hours.";
      $message .= " \nURL: " . $base_url . "node/" . $project->nid;
      error_log($message);
      _ujumbe_notice_mail($subject, $message);
   }
  }
} // function _ujumbe_phone_check

/**
 * Send an notice email to the admin and additional notice addresses
 *
 * @param $subject
 *   Subject line
 * 
 * @param $message
 *   The body of the message
 */
function _ujumbe_notice_mail($subject, $message) {
  $email = variable_get('site_mail', '');
  $notice_email = variable_get('ujumbe_notice_email', '');
  _ujumbe_drupal_mail($email, $email, $subject, $message);
  if($notice_email != '') {
    _ujumbe_drupal_mail($email, $notice_email, $subject, $message);
  }
 
} // function _ujumbe_notice_mail


/**
 * Send an email
 *
 * @param $from
 *   From address
 * 
 * @param $to
 *   To address
 * 
 * @param $subject
 *   Subject line
 * 
 * @param $message
 *   The body of the message
 */
function _ujumbe_drupal_mail($from, $to, $subject, $message) {
  $my_module = 'Ujumbe';
  $my_mail_token = microtime();

  $message = array(
    'id' => $my_module . '_' . $my_mail_token,
    'to' => $to,
    'subject' => $subject,
    'body' => array($message),
    'headers' => array(
      'From' => $from, 
      'Sender' => $from, 
      'Return-Path' => $from,
    ),
  );
  $system = drupal_mail_system($my_module, $my_mail_token);
  $message = $system->format($message);
  if ($system->mail($message)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
} // function _ujumbe_drupal_mail

/**
 * Check to see if a message should go directly to the archive and not recieve and automatic reply.
 *
 * @param $number
 *  The number to be checked
 *
 * @return 
 *  Return true if message should be archived automatically
 */
function _ujumbe_filter_check($number) {
  $filter_min = variable_get('ujumbe_filter_minimum_length', 0); 
  $filter_alpha = variable_get('ujumbe_filter_alpha', 0);
   
  if($filter_min > 0 AND strlen($number) < $filter_min) { 
    return TRUE;
  }
  
  if($filter_alpha == 1 AND !is_numeric($number)) { 
    return TRUE;
  }

  return FALSE;
} // function _ujumbe_filter_check


