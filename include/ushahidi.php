<?php

/**
 * @file
 * The ushahidi file for Ujumbe
 * This file implements Ushahidi push functionality
 **/ 

/**
 * Get the messages for the given project, person and page number
 * 
 * @param $url
 *   The URL to push the message to. This should be a FronlineSMS plugin for Ushahidi URL
 *
 * @param $message
 *   The message to be passed to Ushahidi
 *
 * @param $number
 *   The number of the phone that sent the message
 **/
function _ujumbe_ushahidi_push($url, $message, $number) {
  $encodedMessage = urlencode($message);
  $push_url = $url . "&s=" . $number . "&m=" . $encodedMessage;
  error_log($push_url);
  $ch = curl_init();
  $timeout = 20;
  curl_setopt($ch,CURLOPT_URL,$push_url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $status =curl_exec($ch);
  if ($status != "") {
    error_log("Ushahidi: " . $push_url . " Status: " . $status);
  }
  curl_close($ch);
} // function _ujumbe_ushahidi_push

/**
 * Check if a project has Ushahidi enabled
 *
 * @param $project_id
 *   The id of the project to check
 *
 * @return 
 *   Return true(1) if it's enabled, false(0) if not.
 **/
function _ujumbe_ushahidi_enabled($project_id) {
  $query="SELECT ushahidi_enabled FROM {ist_project} WHERE project_id = $project_id";
  $result = db_query($query);
  $ushahidi_enabled = $result->fetchField();
  if ($ushahidi_enabled==1) { 
    return 1;
  } else {
    return 0;
  }
} // function _ujumbe_ushahidi_enabled

/**
 * Get's the Ushahidi URL to push the message to
 *
 * @param $project_id
 *   The id of the project to get the URL for
 *
 * @return 
 *   The URL. The code strips everything from the &s to the end of line if it exists.
 **/
function _ujumbe_ushahidi_get_url($project_id) {
  $query="SELECT ushahidi_url FROM {ist_project} WHERE project_id = $project_id";
  $result = db_query($query);
  $ushahidi_url= $result->fetchField();
  // Strip everything out of the URL from the &s to end of line
  $clean_url = preg_replace("/\&s.*/", "", $ushahidi_url);
  return $clean_url;
} // function _ujumbe_ushahidi_get_url

/**
 * Processes a message for a project. Pushes it to Ushahidi if the project has it enabled
 *
 * @param $project_id
 *   The id of the project to get the URL for
 *
 * @param $message
 *   The message to be passed to Ushahidi
 *
 * @param $number
 *   The number of the phone that sent the message
 **/
function _ujumbe_ushahidi_process($project_id, $message, $number) {
  $ushahidi_enabled=_ujumbe_ushahidi_enabled($project_id);

  if ($ushahidi_enabled==1) {
   $url = _ujumbe_ushahidi_get_url($project_id);
   _ujumbe_ushahidi_push($url, $message, $number);
  }
} // function _ujumbe_ushahidi_process
