<?php
/**
 * @file
 * The group file for Ujumbe
 * This file implements the group functionality
 */

/**
 * Get all of the groups in the database
 *
 * @param $page
 *   (optional) The page number to get the groups for
 *
 * @return 
 *   A mysql result of the groups 
 */
function _ujumbe_get_groups($page = -1) {
  $max_range = UjumbeTextMessages::getMaxPerPage();
  $query = db_select('ist_group', 'g')
        -> fields('g', array('group_id', 'group_name', 'group_description', 'timestamp'));
  if ($page >=0) {
    $query -> range($page * $max_range,$max_range);
  }
  $query -> orderBy('g.group_id');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_get_groups

/**
 * Get all of the members of a group
 *
 * @param $group_id
 *   The ID of the group
 *
 * @param $page
 *   (optional) The page number to get the groups for
 *
 * @return 
 *   A mysql result of the group members 
 */
function _ujumbe_get_group_members($group_id, $page = -1) {
  $max_range = UjumbeTextMessages::getMaxPerPage();
  $query = db_select('ist_person', 'gp')
        -> fields('gp', array('group_id', 'person_id', 'phone_number', 'timestamp'));
  if ($page >=0) {
    $query -> range($page * $max_range,$max_range);
  }
  $query -> condition('gp.group_id', $group_id, '=');
  $query -> orderBy('gp.person_id');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_get_group_members

/**
 * The details for a given group
 *
 * @param $group_id
 *   The ID of the group
 *
 * @return 
 *   A mysql result with the details of the group
 */
function _ujumbe_group_details($group_id) {
  $query = db_select('ist_group', 'g');
  $query -> fields('g', array('group_id', 'group_name', 'group_description', 'timestamp'));
  $query -> condition('g.group_id', $group_id, '=');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_group_details


/**
 * Gets the name of a group
 *
 * @param $group_id
 *   The ID of the group
 *
 * @returns
 *   The name of the group
 */
function _ujumbe_group_name($group_id) { 
  $result = _ujumbe_group_details($group_id);
  $group = $result->fetchAssoc();
  $name = $group['group_name'];
  $link = l($name, "ujumbe_group_details/$group_id");
  return $link;
} // function _ujumbe_group_name

/**
 * Return the total number of members for the given group
 *
 * @param $group_id 
 *   The ID of the group to return the count for
 *
 * @return
 *   The number of members for the given group
 */
function _ujumbe_member_count($group_id) {
  $query = db_select('ist_person', 'gp');
  $query -> fields('gp', array('person_id'));
  $query -> condition('gp.group_id', $group_id, '=');
  $result = $query -> execute();
  $count = $result -> rowCount();
  return $count;
} // function _ujumbe_member_count

/**
 * Implements hook_form_submit()
 *
 * Updates the details of a group
 */
function _ujumbe_group_details_form_submit($form, &$form_state) {
  $group_id = $form_state['values']['group_id'];
  $group_name = $form_state['values']['group_name'];
  $group_description = $form_state['values']['group_description'];

  $query='UPDATE {ist_group} SET group_name = :group_name, group_description = :group_description WHERE group_id = :group_id';
  $values = array(':group_name' => $group_name, ':group_description' => $group_description, ':group_id' => $group_id);
  db_query($query, $values);

  drupal_set_message(t("Group updated."));
} // function _ujumbe_group_details_form_submit


/**
 * Implements hook_form()
 *
 * A form to edit the details of a group
 */
function _ujumbe_group_details_form($form, &$form_state) {
  $group_id=arg(1);

  $result = _ujumbe_group_details($group_id);
  $group =  $result->fetchAssoc();
 
  $form['group'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['group_id'] = array(
    '#type' => 'hidden',
    '#title' => "group ID",
    '#value' => $group['group_id'],
  );

  $form['group_id_item'] = array(
    '#type' => 'item',
    '#title' => "group ID",
    '#markup' => $group['group_id'],
    '#value' => $group['group_id'],
  );

  if (user_access('access ujumbe admin')) {
    $form['group_name'] = array(
      '#type' => 'textfield',
      '#title' => "group Name",
      '#default_value' => $group['group_name'],
    );
  } else {
    $form['group_name'] = array(
      '#type' => 'item',
      '#title' => "group Name",
      '#markup' => $group['group_name'],
    );
  }

  if (user_access('access ujumbe admin')) {
    $form['group_description'] = array(
      '#type' => 'textarea',
      '#title' => "group description",
      '#default_value' => $group['group_description'],
    );
  } else {
    $form['group_description'] = array(
      '#type' => 'item',
      '#title' => "group description",
      '#markup' => $group['group_description'],
    );
  }

  $form['timestamp'] = array(
    '#type' => 'item',
    '#title' => "Created",
    '#markup' => $group['timestamp'],
  );
  if (user_access('access ujumbe admin')) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => "Submit Changes",
    );
  }
  return $form;
} // function _ujumbe_group_details_form


/**
 * Implements hook_submit()
 *
 * Creates a new group
 */
function _ujumbe_new_group_form_submit($form, &$form_state) {
  $group_name = $form_state['values']['group_name'];
  $group_description = $form_state['values']['group_description'];

  $query='INSERT INTO {ist_group} (group_name, group_description) VALUES (:group_name, :group_description)';
  $values= array(':group_name' => $group_name, ':group_description' => $group_description);
  db_query($query, $values);

  drupal_set_message(t("group Created."));
  $form_state['redirect'] = 'ujumbe_groups';
} // function _ujumbe_new_group_form_submit


/**
 * Implements hook_form()
 *
 * Form to create a new group
 */
function _ujumbe_new_group_form($form, &$form_state) {
  $form['group'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['group_name'] = array(
    '#type' => 'textfield',
    '#title' => "group Name",
  );

  $form['group_description'] = array(
    '#type' => 'textarea',
    '#title' => "group description",
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Create group",
  );
  return $form;
} // function _ujumbe_new_group_form


/**
 * Renders a html page for creating a new group
 *
 * @return
 *   Returns the html for a new group
 */
function _ujumbe_new_group_html_page() {
  $page = "";
  $get_form = drupal_get_form('_ujumbe_new_group_form');
  $page .= drupal_render($get_form);
  return $page;
} // function _ujumbe_new_group_html_page


/**
 * Renders a html page for the details of a group
 *
 * @return
 *   Returns the html for the details of a group
 */
function _ujumbe_group_details_html_page() {
  $group_id=arg(1);
  if(isset($group_id) and $group_id > 0) {

    $page = "";
    $get_form = drupal_get_form('_ujumbe_group_details_form');
    $page .= drupal_render($get_form);
  } 
  $page .= "<b>Members:</b><br>\n";
  $result = _ujumbe_get_group_members($group_id);
  foreach($result as $member) {
    $person_namenum = _ujumbe_person_namenum($member->person_id);
    $page .= $person_namenum . "\n<br>";
  }
  
  $page .= "\n<br>Total: " . _ujumbe_member_count($group_id);

  $page .= "<hr><center><h3>Send this group a message</h3></center>\n";                                           
  $get_form = drupal_get_form('_ujumbe_send_form', "", $group_id, "", "group");
  $page .= drupal_render($get_form);

  return $page;
} // function _ujumbe_group_details_html_page


/** 
 * Renders a html page with a list of all the groups
 *
 * @param $_GET['page']
 *   (optional) The page number
 *
 * @return
 *   Returns the html for listing all the groups
 */
function _ujumbe_groups_html_page() {
  $page_number = 0;
  if (isset($_GET['page']) and $_GET['page'] > 0) {
    $page_number = $_GET['page'];
  }
  $count = _ujumbe_table_count("ist_group");
  $result = _ujumbe_get_groups($page_number);
  $bgcolor = "bgcolor='silver'";

  $page = "";
  $page .= "<h2>groups</h2>(Click on a group to get details)\n";
  $page .= "<table><tr><th>group ID</th><th>group Name</th><th>group Description</th><th>Membership Count</th><th>Created</th></tr>\n";

  foreach ($result as $group) {
    $page .= "<tr $bgcolor>\n";
    $page .= "<td><a href='ujumbe_group_details/$group->group_id'>" . $group->group_id . "</a></td>\n";
    $page .= "<td><a href='ujumbe_group_details/$group->group_id'>" . $group->group_name . "</a></td>\n";
    $page .= "<td><pre><a href='ujumbe_group_details/$group->group_id'>" . $group->group_description. "</a></pre></td>\n";
    $page .= "<td><pre><a href='ujumbe_group_details/$group->group_id'>" . _ujumbe_member_count($group->group_id). "</a></pre></td>\n";
    $page .= "<td width=150><a href='ujumbe_group_details/$group->group_id'>" . $group->timestamp . "</a></td>\n";
    $page .= "</tr>\n";

    if ($bgcolor == "bgcolor='silver'") {
      $bgcolor = "";
    } else {
      $bgcolor = "bgcolor='silver'";
    }
  }

  $page .= "<tr><td>Total: $count\n</td></tr>";
  $page .= "</table>\n";
  $page .= _ujumbe_pager($page_number,$count);
  if(user_access('access ujumbe admin')) {
    $page .= "<a href='ujumbe_new_group'>New group</a>";
  }
  return $page;
} // function _ujumbe_groups_html_page

/**
 * Sends a message a to a group
 * 
 * @param $project_id
 *   The ID of the project
 * @param $group_id
 *   The ID of the group to send to
 * @param $message
 *   The message to send
 *
*/
function _ujumbe_group_send($project_id, $group_id, $message) {                                                                                      
  $numbers="";
  $first = TRUE;

  $result = _ujumbe_get_group_members($group_id);                                                                                                    
  foreach($result as $member) {
    if(!$first) {
      $numbers .= ",";
    }
    $numbers .= $member->phone_number;
    $first = FALSE;
  }
 error_log($numbers);
  _ujumbe_send($project_id, $numbers, $message);
} //function _ujumbe_send

