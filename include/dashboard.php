<?php
/**
 * @file
 * The Dasboard file for Ujumbe
 * This file implements the dashboard functions
 */

use org\drupal\ujumbe\ProjectGateways as UjumbeProjects;

/**
 * Determines the rate of incoming messages in messager per hour.
 * 
 * @return
 *   The number of messages recieved in the last 60 minutes
 */
function _ujumbe_message_rate() {
  $query = "SELECT count(*) FROM {ist_message} WHERE timestamp > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 HOUR)";
  $result = db_query($query);
  $count = $result->fetchField();
  return $count;
} // function ujumbe_message_rate


/**
 * Determines the amount of time since the last message was recieved
 * 
 * @return
 *   The amount of time since the last message was recieved
 */
function _ujumbe_idle_time() {
  $query = "SELECT timediff(now(), timestamp) FROM {ist_message} ORDER BY timestamp DESC LIMIT 1";
  $result = db_query($query);
  $time= $result->fetchField();
  return $time;
} // function ujumbe_idle_time

/**
 * Determines the status of all the active projects
 *
 * @return
 *   The HTML containing info on all projects with less then perfect statuses
 */
function _ujumbe_project_status_html() {
  global $base_url;
  
  $dbProjects = UjumbeProjects::getNew();
  $result = $dbProjects->getActiveProjectGateways();
  $bgcolor = "bgcolor='silver'";
  $current_page = arg(0); 
  $linkroot = "$base_url/ujumbe_project_details/";
  
  $page = "";
  $page .= "<table><tr><th>Gateway Status:</th></tr>\n";

  $gateway_delay_notice = variable_get('ujumbe_gateway_notice_delay', '60') * 60;

  foreach ($result as $project) {
    $gateway_time = strtotime($project->gateway_time);
    $gateway_delay = _ujumbe_elapsed_time($gateway_time);
    $gateway_delay_epoch = time() - $gateway_time;
    
    if ($gateway_delay_notice>0 and $gateway_delay_epoch > $gateway_delay_notice ) {
      $gateway_delay = "<font color='red'>".$gateway_delay."</font>";
    }

    $page .= "<tr $bgcolor>\n";
    $page .= "<td><a href='$linkroot$project->project_id'><b>Project: ". $project->project_id . ". " . $project->project_name . "</b><br>" . $project->gateway_power. "<br>" . $project->gateway_network . "<br>Gateway last seen:<br>" . $gateway_delay. "</a></td>\n";
    $page .= "</tr>\n";

    if ($bgcolor == "bgcolor='silver'") {
      $bgcolor = "";
    } else {
      $bgcolor = "bgcolor='silver'";
    }
  }
  $page .= "</table>";

  return $page;
} // function _ujumbe_project_status_html


/**
 * Implements hook_block_view().
 * 
 * This function displays the dashboard for Ujumbe
 */
function ujumbe_block_view() {
  $total_messages = _ujumbe_message_count();
  $idle_time = _ujumbe_idle_time();
  $message_rate = _ujumbe_message_rate();
  $project_status = _ujumbe_project_status_html();
  $block = array();
  $block['subject'] = t('Ujumbe Dashboard');
  if(user_access('access ujumbe content')){
    $result = "Total Messages: $total_messages <br>";
    $result .= "Time since last message: $idle_time <br>";
    $result .= "Messages per hour: $message_rate <br>";
    $result .= "$project_status<br>";
    $block['content'] = t($result);  
  }
  return $block;
} // function ujumbe_block_view
