<?php

/**
 * @file
 * The people file for Ujumbe
 * This file implements the people functionality for demographics
 */


/**
 * Gets the timestamp from the most recent message from the person
 * 
 * @param $person_id
 *   The ID of the person 
 * 
 * @return 
 *   The timestamp of the most recent message from the person
 */
function _ujumbe_person_recent_timestamp($person_id) {
  $query= "SELECT timestamp FROM {ist_message} WHERE person_id = $person_id ORDER BY timestamp DESC LIMIT 1";
  $result = db_query($query);
  $recent = $result -> fetchField();
  return $recent;
} // function _ujumbe_person_recent_timestamp


/**
 * Get all of the people in the database
 *
 * @param $page
 *   (optional) The page number to get the people for
 *
 * @return 
 *   A mysql result of the people 
 */
function _ujumbe_get_people($page = -1) {
  $max_range = UjumbeTextMessages::getMaxPerPage();
  $query = db_select('ist_person', 'p')
        -> fields('p', array('person_id', 'phone_number', 'name','identifier', 'group_id', 'gender', 'age', 'location', 'timestamp'));
  if ($page >=0) {
    $query -> range($page * $max_range,$max_range);
  }
  $query -> orderBy('p.person_id');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_get_people


/**
 * Get the details for a person
 *
 * @param $person_id
 *   The ID of the person to get details for
 *
 * @return 
 *   A mysql result of the details for the person
 */
function _ujumbe_person_details($person_id) {
  $query = db_select('ist_person', 'p');
  $query -> fields('p', array('person_id', 'phone_number', 'name', 'identifier', 'group_id', 'gender', 'age', 'location', 'timestamp'));
  $query -> condition('p.person_id', $person_id, '=');
  $result = $query -> execute();
  return $result;
} // function _ujumbe_person_details


/**
 * Gets the name or number of a person
 *
 * @param $person_id
 *   The IF of the person
 *
 * @return
 *   Returns the name of the person if it is in the database, otherwise returns the phone number
 */
function _ujumbe_person_namenum($person_id) { 
  $result = _ujumbe_person_details($person_id);
  $person= $result->fetchAssoc();
  $name = $person['name'];
  $number = $person['phone_number'];

  $namenum = $number;

  if($name) {
    $namenum .= " ($name)";
  }
  $link = l($namenum, "ujumbe_person_details/$person_id");
  return $link;
} // function _ujumbe_person_namenum


/**
 * Implements hook_form_subit()
 * 
 * Updates a person's info
 */
function _ujumbe_person_details_form_submit($form, &$form_state) {
  $phone_number = _ujumbe_normal_number($form_state['values']['phone_number']);
  $person_id= $form_state['values']['person_id'];
  $name = $form_state['values']['name'];
  $identifier = $form_state['values']['identifier'];
  $group_id = $form_state['values']['group_id'];
  $gender = $form_state['values']['gender'];
  $age = $form_state['values']['age'];
  $location = $form_state['values']['location'];
  if ($age < 0 or $age == '' or !is_numeric($age)) {
    $age=NULL;
  }

  $query = 'UPDATE {ist_person} SET name = :name, identifier = :identifier, group_id = :group_id, phone_number = :phone_number, gender = :gender, age = :age, location = :location WHERE person_id = :person_id';
  $values = array(':name'=>$name, ':identifier'=>$identifier, ':group_id'=> $group_id, ':phone_number'=> $phone_number, ':gender'=>$gender, ':age'=>$age, ':location'=>$location, ':person_id'=>$person_id);
  db_query($query, $values);

  drupal_set_message(t("Person updated."));

} // function _ujumbe_person_details_form_submit

/**
 * Implements hook_form_subit()
 * 
 * Handles the submit for adding a new person
 */
function _ujumbe_new_person_form_submit($form, &$form_state) {
  $phone_number = $form_state['values']['phone_number'];
  $name = $form_state['values']['name'];
  $identifier = $form_state['values']['identifier'];
  $group_id = $form_state['values']['group_id'];
  $gender = $form_state['values']['gender'];
  $age = $form_state['values']['age'];
  $location = $form_state['values']['location'];
  if ($age < 0 or $age == '' or !is_numeric($age)) {
    $age=NULL;
  }
  _ujumbe_insert_person($phone_number, $location, $name, $identifier, $gender, $age, $group_id);
  drupal_set_message(t("Person Added."));

} // function _ujumbe_new_person_form_submit

/**
 * Implements hook_form_subit()
 * 
 * Handles the submit for importing a csv of people
 */
function _ujumbe_people_import_form_submit($form, &$form_state) {
  // create a csv upload directory in sites/default/files if it doesn't exist 
  // yet
  $filepath = 'public://csvuploads';
  file_prepare_directory($filepath, FILE_CREATE_DIRECTORY);

  // save the uploaded file
  $file = file_save_upload('csvfile', array('file_validate_extensions' => array('CSV')), $filepath, FILE_EXISTS_REPLACE);
  //  drupal_set_message('The file you have uploaded is: <pre>'.print_r($file, TRUE).'</pre>');

  // now read from it
  $realpath = drupal_realpath($file->uri);
  // drupal_set_message("The real path of the file is $realpath");
  $fh = fopen($realpath, 'r');

  // This defines the exact format of the file to be imported.
  $table_header = array('phone_number', 'location', 'name', 'identifier', 'gender', 'age', 'group_id');
  $table = array();
  while(($row = fgetcsv($fh, NULL,',')) !== FALSE){
    if($table_header == NULL) {
      foreach($row as $field) {
        $table_header[] = $field;
      };
      continue;
    };
    $table_row = array();
    for($i = 0; $i < count($table_header) ; $i++) {
      if (isset($row[$i])) {
        $table_row[$table_header[$i]] = $row[$i];
      }
    };
    $table[] = $table_row;

    foreach ($table_header as $header) {
      if (isset($table_row[$header])) {
        $$header = $table_row[$header];
     //drupal_set_message($header . " : " . $$header);
      } else {
        $$header = NULL;
      }
    }
    drupal_set_message("Imported: ($phone_number, $location, $name, $identifier, $gender, $age, $group_id)");
    _ujumbe_insert_person($phone_number, $location, $name, $identifier, $gender, $age, $group_id);
  }
  fclose($fh);
  drupal_set_message("Finished import");
} // function _ujumbe_people_import_form_submit


/**
 * Implements hook_form()
 * 
 * A form to import a CSV of people
 */
function _ujumbe_people_import_form($form, &$form_statte) {
  $form['csvfile'] = array(
    '#type' => 'file',
    '#title' => t('Choose a file'),
  );
  $form['#attributes'] = array(
    'enctype' => "multipart/form-data"
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Import CSV",                                                                                                      
  );
  return $form;
}

/**
 * Implements hook_form()
 * 
 * A form to add a new person
 */
function _ujumbe_new_person_form($form, &$form_statte) {

  $groups_result = _ujumbe_get_groups();
  $group_options = array();                                                           
  $group_options[0] = t("None");
  foreach ($groups_result as $group) {
    $group_options[$group->group_id] = t($group->group_id . ". " . $group->group_name);
  }

  $form['person'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => "Phone Number",
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => "Name",
  );
  $form['identifier'] = array(
    '#type' => 'textfield',
    '#title' => "Identifier",
  );
  $form['group_id'] = array(
    '#type' => 'select',
    '#title' => "Group",
    '#options' => $group_options,
  );

  $form['gender'] = array(
    '#type' => 'textfield',
    '#title' => "Gender",
  );

  $form['age'] = array(
    '#type' => 'textfield',
    '#title' => "Age",
  );

  $form['location'] = array(
    '#type' => 'textfield',
    '#title' => "Location",
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Add Person",
  );
  return $form;
} // function _ujumbe_new_person_form


/**
 * Implements hook_form()
 * 
 * A form to edit the details of a person
 */
function _ujumbe_person_details_form($form, &$form_statte) {
  $person_id=arg(1);

  $result = _ujumbe_person_details($person_id);
  $person =  $result->fetchAssoc();
  $recent = _ujumbe_person_recent_timestamp($person_id);

  $groups_result = _ujumbe_get_groups();
  $group_options = array();                                                           
  $group_options[0] = t("None");
  foreach ($groups_result as $group) {
    $group_options[$group->group_id] = t($group->group_id . ". " . $group->group_name);
  }

  $person['lastmessage'] = $recent;

  $form['person'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
  );

  $form['person_id'] = array(
    '#type' => 'hidden',
    '#title' => "Person ID",
    '#value' => $person['person_id'],
  );

  $form['person_id_item'] = array(
    '#type' => 'item',
    '#title' => "Person ID",
    '#markup' => $person['person_id'],
    '#value' => $person['person_id'],
  );

  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => "Phone Number",
    '#default_value' => $person['phone_number'],
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => "Name",
    '#default_value' => $person['name'],
  );
 $form['identifier'] = array(
    '#type' => 'textfield',
    '#title' => "Identifier",
    '#default_value' => $person['identifier'],
  );
  $form['group_id'] = array(
    '#type' => 'select',
    '#title' => "Group",
    '#options' => $group_options,
    '#default_value' => $person['group_id'],
  );

  $form['gender'] = array(
    '#type' => 'textfield',
    '#title' => "Gender",
    '#default_value' => $person['gender'],
  );

  $form['age'] = array(
    '#type' => 'textfield',
    '#title' => "Age",
    '#default_value' => $person['age'],
  );

  $form['location'] = array(
    '#type' => 'textfield',
    '#title' => "Location",
    '#default_value' => $person['location'],
  );

  $form['timestamp'] = array(
    '#type' => 'item',
    '#title' => "Created",
    '#markup' => $person['timestamp'],
  );

  $form['lastmessage'] = array(
    '#type' => 'item',
    '#title' => "Most Recent Message",
    '#markup' => $person['lastmessage'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Submit Changes",
  );
  return $form;
} // function _ujumbe_person_details_form


/**
 * Renders a html page with a person's details
 *
 * @param arg(1)$person_id
 *   The ID of the person passed as a URL argument
 *
 * @return
 *   The rendered html page to display
 */
function _ujumbe_person_details_html_page() {
  $person_id=arg(1);
  $page = "";
  if(isset($person_id) and $person_id> 0) {
    $phone = _ujumbe_get_person_phone($person_id);
    $get_form = drupal_get_form('_ujumbe_person_details_form');
    $page .= drupal_render($get_form);
    $page .=  _ujumbe_messages_html_page("", $person_id);
    $page .= "<hr><center><h3>Send this person a message</h3></center>\n";
    $get_form = drupal_get_form('_ujumbe_send_form', "", $phone);
    $page .= drupal_render($get_form);
  } 
  return $page;
} // function _ujumbe_person_details_html_page


/**
 * Renders a html page to add a new person
 *
 * @return
 *   The rendered html page to display
 */
function _ujumbe_new_person_html_page() {
  $page = "";
  $get_form = drupal_get_form('_ujumbe_new_person_form');
  $page .= drupal_render($get_form);
  return $page;
} // function _ujumbe_new_person_html_page

/**
 * Renders a html page to import a CSV of people
 *
 * @return
 *   The rendered html page to display
 */
function _ujumbe_people_import_html_page() {
  $page = "";
  $page .= "Please select a CSV to be imported. The formatting of the CSV must be exactly as below.<br><br>";
  $page .= "phone_number,location,name,identifier,gender,age,group_id<br>";

  $get_form = drupal_get_form('_ujumbe_people_import_form');
  $page .= drupal_render($get_form);
  return $page;
} // function _ujumbe_people_import_html_page


/**
 * Renders a html page with a list of people
 *
 * @param $_GET['page']
 *   The page number
 *
 * @return
 *   The rendered html page to display
 */

function _ujumbe_people_html_page() {
  $page_number = 0;
  if (isset($_GET['page']) and $_GET['page'] > 0) {
    $page_number = $_GET['page'];
  }

  $count = _ujumbe_table_count("ist_person");
  $result = _ujumbe_get_people($page_number);

  $bgcolor = "bgcolor='silver'";

  $page = "";
  $page .= "<h2>People</h2>(Click on a person to get details)\n";
  $page .= "<table><tr><th>Person ID</th><th>Phone Number</th><th>Name (Group ID)</th><th>Identifier</th><th>Gender</th><th>Age</th><th>Location</th><th>Created</th></tr>\n";

  foreach ($result as $person) {
    if($person->group_id > 0) { 
      $gid = "($person->group_id)";
    } else {
      $gid = "";
    }
    $page .= "<tr $bgcolor>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->person_id. "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->phone_number. "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->name . " " . $gid . "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->identifier . "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->gender. "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->age. "</a></td>\n";
    $page .= "<td><a href='ujumbe_person_details/$person->person_id'>" . $person->location. "</a></td>\n";
    $page .= "<td width=140><a href='ujumbe_person_details/$person->person_id'>" . $person->timestamp . "</a></td>\n";
    $page .= "</tr>\n";

    if ($bgcolor == "bgcolor='silver'") {
      $bgcolor = "";
    } else {
      $bgcolor = "bgcolor='silver'";
    }
  }

  $page .= "</table>\n";
  $page .= "Total: $count\n";
  $page .= "(" . l('Export People', 'ujumbe_export_people'). ")";
  $page .= _ujumbe_pager($page_number,$count);
  $page .= "<hr>" . l('New Person', 'ujumbe_new_person');
  $page .= "<br>" . l('Import CSV', 'ujumbe_people_import');
  return $page;
} // function _ujumbe_people_html_page


/**
 * This function exports all of the people in the system into a excel .xls file
 * 
 * The reason I went with xls instead of a csv is because csv files technically do 
 * not support anything other then ASCII. So no UTF support.
 * 
 * The file is downloaded to the requesting computer and then the code exits. 
 * So no new page is displayed.
 */
function _ujumbe_export_people_xls(){
  require_once dirname(__DIR__)."/../../libraries/phpexcel/Classes/PHPExcel.php";
  $people= _ujumbe_get_people();

  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getActiveSheet()->setTitle('Ujumbe Messages');
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Person ID')
            ->setCellValue('B1', 'Phone Number')
            ->setCellValue('C1', 'Name')
            ->setCellValue('D1', 'Identifier')
            ->setCellValue('E1', 'Group ID')
            ->setCellValue('F1', 'Gender')
            ->setCellValue('G1', 'Age')
            ->setCellValue('H1', 'Location')
            ->setCellValue('I1', 'Timestamp');
            
  // Loop through the result set
  $rowNumber = 2;
  while ($row = $people->fetchAssoc() !== FALSE) {
     $col = 'A';
     foreach($row as $cell) {
        $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$cell);
        $col++;
     }
     $rowNumber++;
  }

  // Freeze pane so that the heading line won't scroll
  $objPHPExcel->getActiveSheet()->freezePane('A2');

  // Save as an Excel BIFF (xls) file
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment;filename="Ujumbe_People' . date('Ymd') . '.xls"');
 header('Cache-Control: max-age=0');

 $objWriter->save('php://output');
 exit();
} // function _ujumbe_export_people_xls


/**
 * Find the ID of a person based on their phone number
 *
 * @param $phone
 *   The phone number
 *
 * $return 
 *  Returns the ID of the person. Returns -1 if person not found.
 */
function _ujumbe_get_person_id($phone) {
  $phone = str_replace("+", "", $phone);
  $query="SELECT person_id FROM {ist_person} WHERE phone_number = '$phone'";
  $result = db_query($query);
  $person_id = $result->fetchField();
  if ($person_id >= 1) { 
    return $person_id;
  } else {
    return -1;
  }
} // function _ujumbe_get_person_id


/**                                                                                                                       
 * Find the phone number of a person based on their ID
 *
 * @param $id
 *   The id of the person
 *
 * $return 
 *  Returns the phone number of the person. Returns -1 if person not found.
 */
function _ujumbe_get_person_phone($id) {
  $query="SELECT phone_number FROM {ist_person} WHERE person_id = $id";
  $result = db_query($query);
  $phone_number = $result->fetchField();
  if ($phone_number>= 1) { 
    return $phone_number;
  } else {
    return -1;
  }
} // function _ujumbe_get_person_id


/**
 * Insert a new person into the database
 *
 * @param $phone
 *   The phone number of the person
 *
 * @param $location
 *   (optional) The location of the person
 *
 * @return
 *   The ID of the person inserted
function _ujumbe_log_person($phone, $location = null) {
  $number = _ujumbe_normal_number($phone);
  $person_id = _ujumbe_get_person_id($number);
  if ($person_id < 0) {
    $query="INSERT INTO {ist_person} (phone_number, location) VALUES ('$number', '$location');";
    db_query($query);

    $query = "SELECT LAST_INSERT_ID() AS id";
    $result = db_query($query);
    $row = $result->fetchAssoc();
    $person_id = $row['id'];
  }
  return $person_id;
} // function _ujumbe_log_person_location
 */

/**
 * Insert a new person into the database
 *
 * @param $phone
 *   The phone number of the person
 *
 * @param $location
 *   (optional) The location of the person
 *
 * @param $name
 *   (optional) The name of the person
 *
 * @param $identifier
 *   (optional) The identifier of the person
 *
 * @param $gender
 *   (optional) The gender of the person
 *
 * @param $age
 *   (optional) The (int)age of the person
 *
 * @param $group_id
 *   (optional) The (int)group_id of the person
 *
 * @return
 *   The ID of the person inserted
 */
function _ujumbe_insert_person($phone, $location = null, $name = null, $identifier = null, $gender = null, $age = null, $group_id = 0) {
  $phone_number = _ujumbe_normal_number($phone);
  $person_id = _ujumbe_get_person_id($phone_number);
  if ($person_id < 0) {
    $query="INSERT INTO {ist_person} (phone_number, location, name, identifier, gender, age, group_id) VALUES (:phone_number, :location, :name, :identifier, :gender, :age, :group_id);";
    $values= array(':phone_number' => $phone_number, ':location' => $location, ':name' => $name, ':identifier' => $identifier, ':gender' => $gender, ':age' => $age, ':group_id' => $group_id);
    db_query($query, $values);

    $query = "SELECT LAST_INSERT_ID() AS id";
    $result = db_query($query);
    $row = $result->fetchAssoc();
    $person_id = $row['id'];
  }
  return $person_id;
} // function _ujumbe_insert_person
