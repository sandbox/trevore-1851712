<?php
/**
 * @file
 * The translate file for Ujumbe
 * This file implements the translation functionality
 */

use org\drupal\ujumbe\TextMessages as UjumbeTextMessages;

/**
 * Implements hook_form_submit()
 *
 * This updates the translations of messages
 *
 * The IDs of the messages are passed in a space seperated post variable called ids
 */ 
function _ujumbe_translator_form_submit($form, &$form_state) {
  $node_ids= explode(" ", $form_state['values']['ids']);
  $bTranslationUpdated = false;
  // Then use each id to rebuild the individual name of each variable I want to update.
  foreach($node_ids as $nid) {
    if($nid >0) {
      $theNode = node_load($nid);
      $theNode->translation = $form_state['values']["translation_$id"];
      $theNode->notes = $form_state['values']["notes_$id"];
      node_save($theNode);
      $bTranslationUpdated = true;
    }
  }
  if ($bTranslationUpdated) {
    drupal_set_message(t("Translations updated."));
  }
} // function _ujumbe_translator_form_submit

/**
 * Implements hook_form()
 *
 * Form to translate messages
 *
 * @param arg(1)$page_number
 *   The page number you are translating on
 */
function _ujumbe_translator_form() {
  $page_number = arg(1);
  $theTextMsgs = UjumbeTextMessages::getNew();
  $theMsgs = $theTextMsgs->getMessages($page_number);
  $theMsgCount = $theTextMsgs->getMessageCount();

  $form['translate'] = array(
    '#access' => user_access('access ujumbe content'),
    '#type' => 'item',
    '#prefix' => '<table><tr><th>Message ID</th><th width=1000>Message</th>',
    '#suffix' => '</tr>',
  );

  $bgcolor = "bgcolor='silver'";
  $ids = "";
  foreach($theMsgs as $message) {
    $mid = $message->message_id;
    $ids .= "$mid ";
    $form["message_id_$mid"] = array(
      '#type' => 'hidden',
      '#value' => $message->message_id,
      '#prefix' => "<tr $bgcolor><td>",
    );

    $form["mssage_id_item_$mid"] = array(
      '#type' => 'item',
      '#markup' => l($message->message_id, "ujumbe_message_details/$message->message_id"),
      '#suffix' => '</td>',
    );

    $form["message_$mid"] = array(
      '#type' => 'item',
      '#markup' => l($message->message, "ujumbe_message_details/$message->message_id"),
      '#prefix' => '<td>Message: ',
    );
    
    $form["translation_$mid"] = array(
      '#type' => 'textfield',
      '#default_value' => $message->translation,
      '#size' => 100,
      '#prefix' => 'Translation: ',
    );

    $form["notes_$mid"] = array(
      '#type' => 'textfield',
      '#default_value' => $message->notes,
      '#size' => 100,
      '#prefix' => 'Note: ',
      '#suffix' => '</td></tr>',
    );
    if ($bgcolor == "bgcolor='silver'") {
      $bgcolor = "";
    } else {
      $bgcolor = "bgcolor='silver'";
    }

  }
   $form["ids"] = array(
    '#type' => 'hidden',
    '#value' => $ids,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Submit Changes",
    '#prefix' => "<tr><td>Total: $theMsgCount</td><td>",
    '#suffix' => '(Remember to submit before you change pages)</td></tr></table>',
  );

  return $form;
} // function _ujumbe_translator_form

/**
 * Renders a html page for translating messages
 *
 * @param arg(1)$page_number
 *   The page number
 *
 * @return
 *   A html page for translating messages
 */
function _ujumbe_translator_html_page() {
  $page_number = arg(1);
  $theTextMsgs = UjumbeTextMessages::getNew();
  $theMsgCount = $theTextMsgs->getMessageCount();
  $get_form = drupal_get_form('_ujumbe_translator_form');
  $page = drupal_render($get_form);
  $page .= _ujumbe_pager($page_number, $theMsgCount, "arg", "ujumbe_translator");
  return $page;
} // function _ujumbe_translator_html_page
